import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;



public class room_details implements ActionListener {

	private JFrame frmRoomDetails;
	private JTextPane textPane;
	private JLabel lbl1;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	String roomText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					room_details window = new room_details();
					window.frmRoomDetails.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public room_details() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRoomDetails = new JFrame();
		frmRoomDetails.setResizable(false);
		frmRoomDetails.setTitle("Hotel Management System  |  Room Details");
		frmRoomDetails.setBounds(300, 100, 754, 583);
		frmRoomDetails.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRoomDetails.getContentPane().setLayout(null);
		frmRoomDetails.revalidate();
		
		lbl1 = new JLabel("");
		lbl1.setBackground(new Color(47, 79, 79));
		lbl1.setVisible(false);
		lbl1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl1.setBounds(367, 224, 361, 236);
		frmRoomDetails.getContentPane().add(lbl1);
		
		textPane = new JTextPane();
		textPane.setForeground(new Color(47, 79, 79));
		textPane.setFont(new Font("SansSerif", Font.BOLD, 12));
		textPane.setBounds(70, 144, 254, 391);
		frmRoomDetails.getContentPane().add(textPane);
		textPane.setVisible(false);
		textPane.setEditable(false);
		
		
		btnNewButton = new JButton("Luxury Single");
		btnNewButton.addActionListener(new ActionListener() {
			private Scanner rDDScanner;

			public void actionPerformed(ActionEvent e) {
				
				roomText = "Luxury Single Room";
				
				lbl1.setVisible(true);
				lbl1.setOpaque(true);
				lbl1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\LS1.jpg"));
				textPane.setVisible(true);
				textPane.setText("\t  Luxury Single");

				File rDDFile = new File("..\\..\\Hotel\\Change R_Details\\Luxury Single Room.txt");
				
				try {
					rDDScanner = new Scanner(rDDFile);
					
					while (rDDScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+" \n\n"+ rDDScanner.nextLine());
					}
					
					textPane.setText(textPane.getText() +"\n"
							+ "\tRefrigerated mini bar\n"
							+ "\tTea & coffee maker\n"
							+ "\tComplimentary mineral water\r\n"
							+ "\tSpacious, brightly lit closets\n"
							+ "\tIn-room WIFI - free\n"
							+ "\tDigital weighing scale\r\n"
							+ "\tWriting desk & Small Sofa\n\n Bed Type: 1 King Bed\n\n A/C: Yes\n\n Bathroom:  White marble bathrooms with a shower cubicle and bath tub\n\n "
							+ "Location: 6th to 7th floors \n\n View: Looks out over Galle Face Green or the Colombo Harbour\n\n");
					
					btnNewButton.setEnabled(false);
					btnNewButton_1.setEnabled(true);
					btnNewButton_2.setEnabled(true);
					btnNewButton_3.setEnabled(true);
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(null,"ERROR: File Not Found","Alert!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton.setFont(new Font("Tempus Sans ITC", Font.BOLD, 13));
		btnNewButton.setBounds(51, 79, 130, 51);
		frmRoomDetails.getContentPane().add(btnNewButton);
		
		btnNewButton_1 = new JButton("Deluxe Single");
		btnNewButton_1.addActionListener(new ActionListener() {

			private Scanner rDDScanner;

			public void actionPerformed(ActionEvent e) {
				
				roomText="Deluxe Single Room";
				
				textPane.setVisible(true);
				lbl1.setVisible(true);
				lbl1.setOpaque(true);
				lbl1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\DS.jpg"));
				textPane.setText("\t  Deluxe Single");

				File rDDFile = new File("..\\..\\Hotel\\Change R_Details\\Deluxe Single Room.txt");
				
				try {
					rDDScanner = new Scanner(rDDFile);
					
					while (rDDScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+" \n\n"+ rDDScanner.nextLine());
					}
					
					textPane.setText(textPane.getText() +"\n"
							+ "\tRefrigerated mini bar\n"
							+ "\tTea & coffee maker\n"
							+ "\tComplimentary mineral water\r\n"
							+ "\tIn-room WIFI - free\n"
							+ "\tWriting desk & Chair\n\n "
							+ "Bed Type: 1 Bed \n\n "
							+ "A/C: No\n\n "
							+ "Bathroom:  White marble bathrooms with a shower cubicle\n\n "
							+ "Location: 2nd to 3rd floors \n\n "
							+ "View: Looks out over Galle Face Green\n\n");
					
					btnNewButton.setEnabled(true);
					btnNewButton_1.setEnabled(false);
					btnNewButton_2.setEnabled(true);
					btnNewButton_3.setEnabled(true);
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(null,"ERROR: File Not Found","Alert!", JOptionPane.INFORMATION_MESSAGE);
				}

			}
		});
		btnNewButton_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 13));
		btnNewButton_1.setBounds(221, 79, 130, 51);
		frmRoomDetails.getContentPane().add(btnNewButton_1);
		
		btnNewButton_2 = new JButton("Luxury Double");
		btnNewButton_2.addActionListener(new ActionListener() {
			private Scanner rDDScanner;

			public void actionPerformed(ActionEvent e) {
				
				roomText="Luxury Double Room";
				
				lbl1.setVisible(true);
				lbl1.setOpaque(true);
				lbl1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\LD3.jpg"));
				textPane.setVisible(true);				
				textPane.setText("\t  Luxury Double");

				File rDDFile = new File("..\\..\\Hotel\\Change R_Details\\Luxury Double Room.txt");
				
				try {
					rDDScanner = new Scanner(rDDFile);
					
					while (rDDScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+" \n\n"+ rDDScanner.nextLine());
					}
					
					textPane.setText(textPane.getText() + "\n"
							+ "\tRefrigerated mini bar \n"
							+ "\tTea & coffee maker\n"
							+ "\tComplimentary mineral water\n"
							+ "\tSpacious, brightly lit closets\n"
							+ "\tIn-room WIFI - free\n"
							+ "\tDigital weighing scale\r\n"
							+ "\tWriting desk & Sofa\n\n "
							+ "Bed Type: King Twin beds\n\n "
							+ "A/C: Yes\n\n "
							+ "Bathroom:  White marble bathrooms with a shower cubicle and bath tub\n\n "
							+ "Location: 7th to 8th floors \n\n "
							+ "View: Looks out over Galle Face Green or the Colombo Harbour\n\n");
					
					btnNewButton.setEnabled(true);
					btnNewButton_1.setEnabled(true);
					btnNewButton_2.setEnabled(false);
					btnNewButton_3.setEnabled(true);
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(null,"ERROR: File Not Found","Alert!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_2.setFont(new Font("Tempus Sans ITC", Font.BOLD, 13));
		btnNewButton_2.setBounds(395, 79, 130, 51);
		frmRoomDetails.getContentPane().add(btnNewButton_2);
		
		btnNewButton_3 = new JButton("Deluxe Double");
		btnNewButton_3.addActionListener(new ActionListener() {
			private Scanner rDDScanner;

			public void actionPerformed(ActionEvent e) {
				
				roomText="Deluxe Double Room";
				
				lbl1.setVisible(true);
				lbl1.setOpaque(true);
				lbl1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\DD1.jpg"));
				textPane.setVisible(true);
				textPane.setText("\t  Deluxe Double");

				File rDDFile = new File("..\\..\\Hotel\\Change R_Details\\Deluxe Double Room.txt");
				
				try {
					rDDScanner = new Scanner(rDDFile);
					
					while (rDDScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+" \n\n"+ rDDScanner.nextLine());
					}
					
					textPane.setText(textPane.getText() + "\n"
							+ "\tRefrigerated mini bar \n"
							+ "\tTea & coffee maker\n"
							+ "\tComplimentary mineral water\n"
							+ "\tSpacious, brightly lit closets\n"
							+ "\tIn-room WIFI - free\n"
							+ "\tWriting desk & Sofa\n\n"
							+ "Bed Type: Twin beds\n\n"
							+ "A/C: No\n\n "
							+ "Bathroom:  White marble bathrooms with a shower cubicle\n\n "
							+ "Location: 4th to 5th floors \n\n "
							+ "View: Looks out over Galle Face Green\n\n");
					
					btnNewButton.setEnabled(true);
					btnNewButton_1.setEnabled(true);
					btnNewButton_2.setEnabled(true);
					btnNewButton_3.setEnabled(false);
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(null,"ERROR: File Not Found","Alert!", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});
		btnNewButton_3.setFont(new Font("Tempus Sans ITC", Font.BOLD, 13));
		btnNewButton_3.setBounds(568, 79, 130, 51);
		frmRoomDetails.getContentPane().add(btnNewButton_3);
		
		JLabel lblNewLabel_1 = new JLabel("Room Types:");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 22));
		lblNewLabel_1.setBounds(10, 36, 164, 32);
		frmRoomDetails.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("HOTEL Alizia");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setForeground(new Color(220, 20, 60));
		lblNewLabel_2.setFont(new Font("Snap ITC", Font.BOLD, 28));
		lblNewLabel_2.setBounds(427, 0, 301, 51);
		frmRoomDetails.getContentPane().add(lblNewLabel_2);
		
		
		JButton btnNewButton_4 = new JButton("HOME");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				home_page window = new home_page();
				window.frmHotelAlizia.setVisible(true);
				frmRoomDetails.dispose();
			}
		});
		btnNewButton_4.setForeground(new Color(165, 42, 42));
		btnNewButton_4.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
		btnNewButton_4.setBounds(557, 483, 141, 38);
		frmRoomDetails.getContentPane().add(btnNewButton_4);
		
		JLabel lblNewLabel = new JLabel(" ");
		lblNewLabel.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\b2_1.jpg"));
		lblNewLabel.setBounds(0, 0, 750, 550);
		frmRoomDetails.getContentPane().add(lblNewLabel);
		
	}

	@Override  
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
