import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

public class home_page implements ActionListener{

	JFrame frmHotelAlizia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					home_page window = new home_page();
					window.frmHotelAlizia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public home_page() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHotelAlizia = new JFrame();
		frmHotelAlizia.setResizable(false);
		frmHotelAlizia.setTitle("Hotel Management System");
		frmHotelAlizia.setBounds(300, 100, 740, 540);
		frmHotelAlizia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHotelAlizia.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Room Details");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				room_details window = new room_details();
//				window.frame.setVisible(true);
				frmHotelAlizia.dispose();
				
				
			}
		});
		btnNewButton.setBorder(new LineBorder(new Color(47, 79, 79), 3, true));
		btnNewButton.setBackground(UIManager.getColor("Button.light"));
		btnNewButton.setFont(new Font("Segoe Print", Font.BOLD, 12));
		btnNewButton.setForeground(new Color(0, 0, 128));
		btnNewButton.setBounds(47, 378, 197, 56);
		frmHotelAlizia.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Exit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main_login_page window = new main_login_page();
				window.frmWelcome.setVisible(true);
				frmHotelAlizia.dispose();
			}
		});
		btnNewButton_1.setFont(new Font("MS UI Gothic", Font.BOLD, 15));
		btnNewButton_1.setForeground(new Color(178, 34, 34));
		btnNewButton_1.setBounds(599, 444, 81, 35);
		frmHotelAlizia.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Booking");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				Booking window = new Booking();
//				window.frame.setVisible(true);
				frmHotelAlizia.dispose();
			}
		});
		btnNewButton_2.setBorder(new LineBorder(new Color(47, 79, 79), 3, true));
		btnNewButton_2.setFont(new Font("Segoe Print", Font.BOLD, 12));
		btnNewButton_2.setForeground(new Color(0, 0, 128));
		btnNewButton_2.setBounds(294, 378, 197, 56);
		frmHotelAlizia.getContentPane().add(btnNewButton_2);
		
		JLabel lblNewLabel_1 = new JLabel("<html> Located on one of the largest stretch of the west coasts; "
				+ "Hotel Alizia invites you to experience the best of the Sri Lanka. "
				+ "Discover lush gardens, tranquil pools, exquisite dining, and high-end luxury "
				+ "accommodations that channel the spirit of the region. "
				+ "whichever you pick, you will enjoy premium amenities, thoughtful service, "
				+ "and one of the most beautiful settings imaginable.\r\n</html>");
		lblNewLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setForeground(new Color(255, 228, 225));
		lblNewLabel_1.setBounds(17, 100, 455, 114);
		frmHotelAlizia.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("W  E  L  C  O  M  E  !!");
		lblNewLabel_2.setForeground(new Color(255, 250, 240));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Showcard Gothic", Font.BOLD, 30));
		lblNewLabel_2.setBounds(198, 23, 328, 51);
		frmHotelAlizia.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\ph1.jpg"));
		lblNewLabel.setBounds(476, 94, 238, 248);
		frmHotelAlizia.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				home_page window = new home_page();
//				window.frmHotelAlizia.setVisible(true);
				frmHotelAlizia.dispose();
			}
		});
		btnNewButton_3.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\Manager icon.png"));
		btnNewButton_3.setForeground(new Color(128, 0, 0));
		btnNewButton_3.setFont(new Font("Segoe UI Symbol", Font.BOLD, 11));
		btnNewButton_3.setBounds(630, 23, 65, 51);
		frmHotelAlizia.getContentPane().add(btnNewButton_3);
		
		JLabel lblNewLabel_1_1 = new JLabel("<html> Our dreamy resort is perfect for romantic retreats, "
				+ "family getaways, and spectacular meetings and events. "
				+ "Experience Hotel Alizia and let authentic Sri Lankan hospitality and the magic of "
				+ "west coast and white sand beach sweep you off your feet.</html>");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1.setForeground(new Color(255, 228, 225));
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1_1.setBounds(47, 225, 413, 122);
		frmHotelAlizia.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\bg1.jpg"));
		lblNewLabel_3.setBounds(0, 0, 740, 540);
		frmHotelAlizia.getContentPane().add(lblNewLabel_3);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	}
}
