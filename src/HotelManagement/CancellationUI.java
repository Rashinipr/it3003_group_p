package HotelManagement;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;


public class CancellationUI {

	private JFrame frame_2;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CancellationUI window = new CancellationUI();
					window.frame_2.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CancellationUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame_2 = new JFrame();
		frame_2.getContentPane().setBackground(new Color(60, 179, 113));
		frame_2.getContentPane().setForeground(new Color(50, 205, 50));
		frame_2.setBounds(100, 100, 408, 445);
		frame_2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_2.getContentPane().setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(78, 108, 224, 36);
		textField_1.setBackground(new Color(0, 255, 127));
		textField_1.setBorder(null);
		frame_2.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(78, 155, 224, 36);
		textField_2.setBackground(new Color(0, 255, 127));
		textField_2.setBorder(null);
		frame_2.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		TitledBorder titled = new TitledBorder("NIC No");
		textField_1.setBorder(titled);
		
		TitledBorder titled1 = new TitledBorder("PassCode");
		textField_2.setBorder(titled1);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(78, 202, 224, 45);
		dateChooser.setBackground(new Color(0, 255, 127));
		dateChooser.setBorder(null);
		frame_2.getContentPane().add(dateChooser);
		
		TitledBorder titled2 = new TitledBorder("Date");
		dateChooser.setBorder(titled2);
		
		JLabel lblNewLabel_4 = new JLabel(" Cancel Booking");
		lblNewLabel_4.setBounds(106, 37, 211, 45);
		lblNewLabel_4.setForeground(Color.BLACK);
		lblNewLabel_4.setFont(new Font("Segoe UI Variable", Font.PLAIN, 23));
		frame_2.getContentPane().add(lblNewLabel_4);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.setBounds(196, 268, 106, 23);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 try {
				    	
					    
					    
					    //String ms4 = comboBox.getSelectedItem().toString();
					    //System.out.println(ms1+ms2+ms3);
					    
					    //BookingCancel cancel2 = new BookingCancel();
					    BookingCancellation user = new BookingCancellation();
					    user = new BookingCancellation();
				        
					    
				        user.setPassCode(Integer.parseInt(textField_2.getText()));
				        BookingCancellation.setNIC_No(textField_1.getText());
				        BookingCancellation.setDateChooser_2(dateChooser.getDate());
				        
				        
	     			    //BookingCancel cancel = new BookingCancel(ms2,ms3,giveDate1);
	     			    BookingCancel cancel2 = new BookingCancel(user);
	     			    
	     				//textPane.setText("No Rooms Booked");
	     					
	     					
	     					
	     				
					
	     			
				    }catch(Exception ex) {
		 			
		 			    JOptionPane.showMessageDialog(btnNewButton, "Invalid Input!","Error Message",JOptionPane.ERROR_MESSAGE);
//		 			    RoomNo.setText(null);
//		 			    dateChooser_2.setDate(null);
		 			
		 		    }
				    
			}
		});
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setFont(new Font("Segoe UI Variable", Font.BOLD, 11));
		btnNewButton.setBackground(new Color(0, 255, 127));
		btnNewButton.setBorder(null);
		frame_2.getContentPane().add(btnNewButton);
		
//		JLabel lblNewLabel = new JLabel("");
//		lblNewLabel.setBounds(24, 279, 111, 116);
//		Image Img= new ImageIcon(this.getClass().getResource("/Img/password-icon.png")).getImage();
//		lblNewLabel.setIcon(new ImageIcon(Img));
//		frame_2.getContentPane().add(lblNewLabel);
		
//		JLabel lblNewLabel_5 = new JLabel("");
//		lblNewLabel_5.setBounds(72, 37, 217, 262);
//		Image Img1= new ImageIcon(this.getClass().getResource("/Img/hotel_1.jpg")).getImage();
//		lblNewLabel_5.setIcon(new ImageIcon(Img1));
//		frame_2.getContentPane().add(lblNewLabel_5);
//		
		JButton btnNewButton_1 = new JButton("Report");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Report us = new Report();
			    us.report();
			}
		});
		btnNewButton_1.setBounds(196, 302, 106, 23);
		btnNewButton_1.setForeground(Color.BLACK);
		btnNewButton_1.setFont(new Font("Segoe UI Variable", Font.BOLD, 11));
		btnNewButton_1.setBackground(new Color(0, 255, 127));
		btnNewButton_1.setBorder(null);
		frame_2.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Search PassCode");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchPasscode can = new SearchPasscode();
				can.frame_1.setVisible(true);
				
			}
		});
		btnNewButton_2.setBounds(196, 336, 106, 23);
		btnNewButton_2.setForeground(Color.BLACK);
		btnNewButton_2.setFont(new Font("Segoe UI Variable", Font.BOLD, 11));
		btnNewButton_2.setBackground(new Color(0, 255, 127));
		btnNewButton_2.setBorder(null);
		frame_2.getContentPane().add(btnNewButton_2);
	}
}
