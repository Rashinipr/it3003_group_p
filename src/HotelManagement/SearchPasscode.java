package HotelManagement;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;


public class SearchPasscode {

	private JFrame frame;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchPasscode window = new SearchPasscode();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SearchPasscode() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(188, 143, 143));
		frame.getContentPane().setForeground(new Color(50, 205, 50));
		frame.setBounds(100, 100, 353, 445);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBackground(new Color(102, 205, 170));
		textField_1.setBounds(80, 108, 170, 36);
		textField_1.setBorder(null);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBackground(new Color(102, 205, 170));
		textField_2.setBounds(80, 155, 170, 36);
		textField_2.setBorder(null);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		TitledBorder titled = new TitledBorder("NIC No");
		textField_1.setBorder(titled);
		
		TitledBorder titled1 = new TitledBorder("Room No");
		textField_2.setBorder(titled1);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBackground(new Color(102, 205, 170));
		dateChooser.setBounds(80, 202, 170, 45);
		dateChooser.setBorder(null);
		frame.getContentPane().add(dateChooser);
		
		TitledBorder titled2 = new TitledBorder("Check in date");
		dateChooser.setBorder(titled2);
		
		JLabel lblNewLabel_4 = new JLabel(" Seach PassCode");
		lblNewLabel_4.setForeground(Color.BLACK);
		lblNewLabel_4.setFont(new Font("Segoe UI Variable", Font.PLAIN, 23));
		lblNewLabel_4.setBounds(78, 37, 211, 45);
		frame.getContentPane().add(lblNewLabel_4);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				 getVariables obj1 = new getVariables();
				    obj1 = new getVariables();
			        
				    
				    getVariables.setNICNo(textField_1.getText());
				    obj1.setRoomNo(Integer.parseInt(textField_2.getText()));
				    getVariables.setDateChooser(dateChooser.getDate());
				    
				    SearchPasscode obj2 = new SearchPasscode();
				    obj2.passCodeSearch(obj1);
				}catch(Exception ex) {
		 			
	 			    JOptionPane.showMessageDialog(btnNewButton, "Invalid Input!","Error Message",JOptionPane.ERROR_MESSAGE);
//	 			    RoomNo.setText(null);
//	 			    dateChooser_2.setDate(null);
	 			
	 		    }
				    
			}
		});
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setFont(new Font("Segoe UI Variable", Font.BOLD, 11));
		btnNewButton.setBackground(new Color(102, 205, 170));
		btnNewButton.setBounds(159, 261, 89, 23);
		btnNewButton.setBorder(null);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(24, 279, 111, 116);
		Image Img= new ImageIcon(this.getClass().getResource("/Img/password-icon.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(Img));
		frame.getContentPane().add(lblNewLabel);
		
//		JLabel lblNewLabel_5 = new JLabel("");
//		lblNewLabel_5.setBounds(60, 37, 217, 262);
//		Image Img1= new ImageIcon(this.getClass().getResource("/Img/hotel_1.jpg")).getImage();
//		lblNewLabel_5.setIcon(new ImageIcon(Img1));
//		frame.getContentPane().add(lblNewLabel_5);
	}
    public static class getVariables {
    	
        private static String NICNo;
        private static int RoomNo;
        private static Date dateChooser;


    	public static String getNICNo() {
    		return NICNo;
    	}

    	public static void setNICNo(String nICNo) {
    		if(nICNo.length()>=9) {
    			getVariables.NICNo = nICNo;}
    		else {
    		}
    	}
    	public static int getRoomNo() {
    		return RoomNo;
    	}

    	public void setRoomNo(int roomNo) {
    		getVariables.RoomNo = roomNo;
    	}

    	public static Date getDateChooser() {
    		return dateChooser;
    	}

    	public static void setDateChooser(Date dateChooser_2) {
    		getVariables.dateChooser = dateChooser_2;
    	}	
	}
	public  void passCodeSearch(getVariables obj1) {
		
		String a2=getVariables.getNICNo();
		int a3=getVariables.getRoomNo();
		Date a4=getVariables.getDateChooser();
		
		String  a5 = a2.substring(0, 9);
		
		DateFormat dff = new SimpleDateFormat("yyyy-MM-dd");
		String   gD=dff.format(a4);  
		LocalDate a6 = LocalDate.parse(gD);
		
		File myFile_5 = null;
		StringBuffer stringBufferOfData3 = new StringBuffer();
		
		Scanner fileToRead3 = null;
  		stringBufferOfData3.delete(0, stringBufferOfData3.length());
		try {
				myFile_5=new File("..\\..\\Hotel\\C_Credentials\\"+a5+"_"+a6+".txt");
				fileToRead3 = new Scanner(myFile_5); //point the scanner method to a fill
	            for (String line_5; fileToRead3.hasNextLine() && (line_5 = fileToRead3.nextLine()) != null; ) {
	                stringBufferOfData3.append(line_5).append("\r\n");   
	            }
	            fileToRead3.close();

	    		String b1=String.valueOf("Room No. "+a3+" - ");
	            String lineToEdit3      = b1;//read the line to edit
                int startIndex3 = stringBufferOfData3.indexOf(lineToEdit3);
	            int endIndex3   = startIndex3 + lineToEdit3.length();
	            String  b2 = stringBufferOfData3.substring(endIndex3, endIndex3+3);
	            
	            String b3=String.valueOf(a3);
                int p=b3.length();
	   		    if(p==1) {
	   		    	String b4=String.valueOf("Room No. ");
		            String lineToEdit4      = b4;//read the line to edit
	                int startIndex4 = stringBufferOfData3.indexOf(lineToEdit4);
		            int endIndex4   = startIndex4 + lineToEdit4.length();
		            String  b5 = stringBufferOfData3.substring(endIndex4, endIndex4+1);
		            
		            int b6=Integer.parseInt(b5);
		            if(b6==a3) {
		            	JOptionPane.showMessageDialog(null,"Your Pass Code is :" +b2,"Pass Code",JOptionPane.INFORMATION_MESSAGE);
		   		    }else {
		   		    	JOptionPane.showMessageDialog(null, "Room no is invalid! " ,"Error Message",JOptionPane.ERROR_MESSAGE);
		   		    }
	   		    }else if(p==2){
	   		    	String b4=String.valueOf("Room No. ");
		            String lineToEdit4      = b4;//read the line to edit
	                int startIndex4 = stringBufferOfData3.indexOf(lineToEdit4);
		            int endIndex4   = startIndex4 + lineToEdit4.length();
		            String  b5 = stringBufferOfData3.substring(endIndex4, endIndex4+2);
		            
		            int b6=Integer.parseInt(b5);
		            if(b6==a3) {
		            	JOptionPane.showMessageDialog(null,"Your Pass Code is :" +b2,"Pass Code",JOptionPane.INFORMATION_MESSAGE);
		   		    }else {
		   		    	JOptionPane.showMessageDialog(null, "Room no is invalid! " ,"Error Message",JOptionPane.ERROR_MESSAGE);
		   		    }
	   		    }else {
	   		    	JOptionPane.showMessageDialog(null, "Room no is invalid! " ,"Error Message",JOptionPane.ERROR_MESSAGE);
	   		    }
	   		    
        } catch (FileNotFoundException ex) {//if the file cannot be found 
            
            JOptionPane.showMessageDialog(null, "NIC no or Check in date is invalid! " ,"Error Message",JOptionPane.ERROR_MESSAGE);

        } finally {
	    }
	}
	}

