package HotelManagement;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime; 

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

public class BookingCancel {
	 
	private String ms4;
	private LocalDate b;
	private DateTimeFormatter formatter;
	private LocalDate f;
	
	public BookingCancel(BookingCancellation user)  {
		
		int ms2 = BookingCancellation.getPassCode();
	    String ms3=BookingCancellation.getNIC_No();
	    Date giveDate1=BookingCancellation.getDateChooser_2();
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = new Date();
		String gD3=df.format(d1);
		LocalDate d = LocalDate.parse(gD3);
		
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		String   gD1=df1.format(giveDate1);  
		LocalDate a = LocalDate.parse(gD1);
		
		String  c = ms3.substring(0, 5);
		
		
		if (a.compareTo(d)>= 0  ) {
			Path path = Paths.get("..\\..\\Hotel\\C_Unbooking\\"+ms2+"_"+c+".txt");
			if(Files.exists(path)) { 
	//==============================================================================			
				File file1 = new File("..\\..\\Hotel\\C_Unbooking\\"+ms2+"_"+c+".txt");
	            InputStreamReader streamReader = null;
				try {
					streamReader = new InputStreamReader(new FileInputStream(file1));
				} catch (FileNotFoundException e1) {
					//e1.printStackTrace();
				}
	            BufferedReader br = new BufferedReader(streamReader);

		      	String giveDate2 = new String();
		      	String roomNo = new String();
		      	String roomtype = new String();
		      	String giveDate3 = new String();
		      	
		      	  try {  
		      		roomNo = br.readLine();
		      		roomtype = br.readLine();
		      		giveDate3 = br.readLine();
		      		
					while (br.ready()) {
						giveDate2 = br.readLine();
					  }
					br.close();
				  }catch (IOException e) {
					  
					e.printStackTrace();
				  }
    //===============================================================================
		      	formatter = DateTimeFormatter.ISO_DATE; 
		      	f=LocalDate.parse(giveDate3, formatter);//System.out.println(f);
	//===============================================================================
		      	int ms1=Integer.parseInt(roomNo);
	//===============================================================================	      	
		      	
		      	b = LocalDate.parse(giveDate2, formatter); 
		      	
	//===============================================================================
		      	
		      	newVariables us = new newVariables();
			    us = new newVariables();
		        
		        us.setA(a);
		        us.setMs1(ms1);
		        us.setC(c);
		      	us.setB(b);
		      	us.setF(f);
	//===============================================================================	
		      	if(a.compareTo(f)> 0 && b.compareTo(f)> 0 ) {
		      		try {
						if((ms1!=0 && ms1<41) && (ms2!=0&&ms2<1000)) {
							
							if(ms1>0 && ms1<11) {
								
									ms4="Luxury Single Room";
									LuxurySingleRoom a1 =new LuxurySingleRoom();
									a1.CancelProcess2(ms4);		
							}
							else if(ms1>10 && ms1<21) {
								
									ms4="Deluxe Single Room";
									DeluxSingleRoom a1 =new DeluxSingleRoom();
									a1.CancelProcess2(ms4);	
						    } 	
							else if(ms1>20 && ms1<31) {
								
									ms4="Luxury Double Room";
									LuxuryDoubleRoom a1 =new LuxuryDoubleRoom();
									a1.CancelProcess2(ms4);	
							}
							else if(ms1>30 && ms1<41) {
								
									ms4="Deluxe Double Room";
									DeluxDoubleRoom a1 =new DeluxDoubleRoom();
									a1.CancelProcess2(ms4);	
							}
//							else {
//								JOptionPane.showMessageDialog(null,"Invalid Input","Error",JOptionPane.ERROR_MESSAGE);}
//							}
						else {
							JOptionPane.showMessageDialog(null,"PassCode not in correct formate","Error",JOptionPane.ERROR_MESSAGE);}
						}
						
						}catch(Exception e)
						{
							//JOptionPane.showMessageDialog(null,"Pass_code not in correct formate","Error",JOptionPane.ERROR_MESSAGE);
							}
						}
		      	else {
			    	JOptionPane.showMessageDialog(null,"Invalid Date","Error",JOptionPane.ERROR_MESSAGE);}
			    }
			
		    else {
		    	JOptionPane.showMessageDialog(null,"Room not Booked or Invalid details","Error",JOptionPane.ERROR_MESSAGE);}
		    }
		else {
			JOptionPane.showMessageDialog(null,"Entered date is already gone","Error",JOptionPane.ERROR_MESSAGE);}
		}
//===============================================================================================================================			
}
class newVariables{
	
	private static String c_new;
	private static int ms1_new;
	private static LocalDate b_new;
	private static LocalDate a_new;
	private static LocalDate f_new;
	
	public static LocalDate getA() {
		return a_new;
	}
	public void setA(LocalDate a) {
		newVariables.a_new = a;
	}
	public static int getMs1() {
		return ms1_new;
	}
	public void setMs1(int ms1) {
		newVariables.ms1_new = ms1;
	}
	public static String getC() {
		return c_new;
	}
	public void setC(String c) {
		newVariables.c_new = c;
	}
	public static LocalDate getB() {
		return b_new;
	}
	public void setB(LocalDate b) {
		newVariables.b_new = b;
	}
	public static LocalDate getF() {
		return f_new;
	}
	public void setF(LocalDate f) {
		newVariables.f_new = f;
	}

}
		
//======================================================================================================================================

class Room     
{
	static File myFile_1 = null;
	static StringBuffer stringBufferOfData = new StringBuffer();
	
	static File myFile_3 = null;
	static StringBuffer stringBufferOfData1 = new StringBuffer();
	
	static File myFile_4 = null;
	static StringBuffer stringBufferOfData2 = new StringBuffer();
	
	static File myFile_6 = null;
	static StringBuffer stringBufferOfData6 = new StringBuffer();
	
	String c=newVariables.getC();
	LocalDate a=newVariables.getA();
	LocalDate b=newVariables.getB();
	LocalDate f=newVariables.getF();
	int ms1=newVariables.getMs1();
	int ms2=BookingCancellation.getPassCode();
	
	
	void CancelProcess2(String ms4) throws IOException 
	{
		
		if (b.compareTo(a)>= 0  ) {
			
			
			
			File R1 = new File("C:\\CancellationReport\\R1.txt");
			File R2 = new File("C:\\CancellationReport\\R2.txt");
			File R3 = new File("C:\\CancellationReport\\R3.txt");
			File R4 = new File("C:\\CancellationReport\\R4.txt");

		    List<LocalDate> da1 = a.datesUntil(b.plusDays(1)).collect(Collectors.toList());
		    List<LocalDate> da4 = f.datesUntil(b.plusDays(1)).collect(Collectors.toList());
				for(int j = 1; j<6; j++) {
					
					switch(j)
					{
					    case 1:
						   Room cb = new Room();
						   cb.runProcess1(ms1,ms2,a,b,c,ms4);				
					       break;
                        case 2:
                        	for (int i=0;  i < da1.size();i++) {
						    	LocalDate da2 = da1.get(i);
						    	Path path0 = Paths.get("..\\..\\Hotel\\Unbooked\\"+ms4+"\\"+da2+".txt");
								try {
									if(Files.exists(path0)) {
										Room cc = new Room();
										cc.runProcess2(ms1,da2,ms4);	
									}
								}catch(StringIndexOutOfBoundsException e1) {
									//JOptionPane.showMessageDialog(frame, e1.getMessage());
									//JOptionPane.showMessageDialog(null,"Invalid Input","Error",JOptionPane.ERROR_MESSAGE);}
									//System.out.println("");
								}	
						    }					
					       break;       
					    case 3:
							for (int i=0;  i < da1.size();i++) {
						    	LocalDate da2 = da1.get(i);
						    	Path path1 = Paths.get("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+da2+".txt");
								try {
									if(Files.exists(path1)) {
										Room ca = new Room();
								        ca.runProcess(ms1,da2,ms4);	
									}
								}catch(StringIndexOutOfBoundsException e1) {
									//JOptionPane.showMessageDialog(frame, e1.getMessage());
									//JOptionPane.showMessageDialog(null,"Invalid Input","Error",JOptionPane.ERROR_MESSAGE);}
									//System.out.println("");
								}	
						    }	
							break;
						case 4:
							for (int i=0;  i < da1.size();i++) {
						    	LocalDate da2 = da1.get(i);
						    	ms4=("AllRooms");
						    	Path path2 = Paths.get("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+da2+".txt");								
								try {
									if(Files.exists(path2)) {
										Room ca = new Room();
								        ca.runProcess(ms1,da2,ms4);    									
									}
								}catch(StringIndexOutOfBoundsException e1) {
									//JOptionPane.showMessageDialog(frame, e1.getMessage());
									//JOptionPane.showMessageDialog(null,"Invalid Input","Error",JOptionPane.ERROR_MESSAGE);}
									//System.out.println("");
								}	
						    }	
							break;
						case 5:
							for (int i=0;  i < da4.size();i++) {
								LocalDate da2 = da4.get(i);
						    	ms4=("RoomCdetails");
						    	Path path3 = Paths.get("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+ms1+"_"+da2+".txt");							 							
								try {
									if(Files.exists(path3)) {
										//File myFile_2=new File("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+ms1+"_"+da5+".txt");
										if(a.compareTo(da2)> 0 ) {
											Room ca = new Room();
									        ca.runProcess(ms1,da2,ms4); 
										}
										else if(da2.compareTo(a)>=0) {
											File myFile_2=new File("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+ms1+"_"+da2+".txt");
											  
										    if(myFile_2.delete() )
										    {
										    	FileWriter f3 = new FileWriter("C:\\CancellationReport\\R2.txt",true);
								    			f3.write(myFile_2+" deleted"+"\n");
								    			f3.close();
										    	//JOptionPane.showMessageDialog(null, "Booking Cancelled Successfully","Message",JOptionPane.INFORMATION_MESSAGE);
										    }
										    else
										    {
										    	//JOptionPane.showMessageDialog(null, "Booking Cancelled Unsuccessfully","Message",JOptionPane.INFORMATION_MESSAGE);
										    }    	
										}
									    							
									}
								}catch(StringIndexOutOfBoundsException e1) {
									//JOptionPane.showMessageDialog(frame, e1.getMessage());
									JOptionPane.showMessageDialog(null,"Invalid Input-1","Error",JOptionPane.ERROR_MESSAGE);
									//System.out.println("");
								}
							}
//							for (int i=0;  i < da1.size();i++) {
//								LocalDate da2 = da1.get(i);
//						    	ms4=("RoomCdetails");
//						    	Path path3 = Paths.get("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+ms1+"_"+da2+".txt");							 							
//								try {
//									if(Files.exists(path3)) {
//																	
//									}
//								}catch(StringIndexOutOfBoundsException e1) {
//									//JOptionPane.showMessageDialog(frame, e1.getMessage());
//									//JOptionPane.showMessageDialog(null,"Invalid Input","Error",JOptionPane.ERROR_MESSAGE);}
//									//System.out.println("");
//								}
//							}	
							break;
					   }	
				}//JOptionPane.showMessageDialog(null, "Booking Cancelled successfully","Message",JOptionPane.INFORMATION_MESSAGE);   	
		}	
	}
//==============================================================================================================================================================================================================================
    void runProcess(int ms1,LocalDate da2,String ms4) throws IOException {
    	
  		Scanner fileToRead = null;
  		stringBufferOfData.delete(0, stringBufferOfData.length());
  		
  		Scanner fileToRead6 = null;
  		stringBufferOfData6.delete(0, stringBufferOfData6.length());
  		if(ms4!="RoomCdetails") {
  			try {
				myFile_1=new File("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+da2+".txt");
				fileToRead = new Scanner(myFile_1); //point the scanner method to a fill
	            for (String line; fileToRead.hasNextLine() && (line = fileToRead.nextLine()) != null; ) {
	                stringBufferOfData.append(line).append("\r\n");   
	            }
	            Room ca = new Room();
                ca.replacement(ms1,da2,ms4);    
	         
	            fileToRead.close();
	   		    
        } catch (FileNotFoundException ex) {//if the file cannot be found 
            
            JOptionPane.showMessageDialog(null, "The file " + myFile_1 + " could not be found! " ,"Error Message",JOptionPane.ERROR_MESSAGE);

        } finally {
            fileToRead.close();
            
        }  
		
  		}
  		if(ms4=="RoomCdetails") {
  			try {
				myFile_6=new File("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+ms1+"_"+da2+".txt");
				fileToRead6 = new Scanner(myFile_6); //point the scanner method to a fill
	            for (String line6; fileToRead6.hasNextLine() && (line6 = fileToRead6.nextLine()) != null; ) {
	                stringBufferOfData6.append(line6).append("\r\n");   
	            }
	            Room ca = new Room();
                ca.replacement(ms1,da2,ms4);    
	         
	            fileToRead6.close();
	   		    
        } catch (FileNotFoundException ex) {//if the file cannot be found 
            
            JOptionPane.showMessageDialog(null, "The file " + myFile_6 + " could not be found! " ,"Error Message",JOptionPane.ERROR_MESSAGE);

        } finally {
            fileToRead6.close();
            
        }  
		
  		}
		

  	}   
//	void deleteFile(LocalDate da2, String ms4) throws IOException {
//		File myFile_7=new File("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+da2+".txt");
//		//if (myFile_1.exists()) {
//		try {
//			Scanner myReader = new Scanner(myFile_7);
//			while (myReader.hasNextLine()) {
//			String data = myReader.nextLine();
//			//System.out.println(data.length());
//			if (data.length() == 0) {
//	        	myFile_7.deleteOnExit();
//	        	
//	        	FileWriter f2 = new FileWriter("C:\\CancellationReport\\R1.txt",true);
//    			f2.write(myFile_1+" deleted"+"\n");
//    			f2.close();
//			}
//			}
//			myReader.close();
//			} catch (FileNotFoundException e) {
//			//e.printStackTrace();
//			}
//		
//    }
		
    void replacement(int ms1,LocalDate da2, String ms4) throws IOException {
    	if(ms4!="RoomCdetails") {
    		String s=String.valueOf(ms1);
            String lineToEdit = (s+",");//read the line to edit
            String replacementText = "";//read the line to replace
            

            int startIndex = stringBufferOfData.indexOf(lineToEdit);
            int endIndex = startIndex + lineToEdit.length();
           
            
            stringBufferOfData.replace(startIndex, endIndex, replacementText);
            
            try {
    			FileWriter myWriter = new FileWriter(myFile_1);
    			myWriter.write(stringBufferOfData.toString());
    		
    			FileWriter f2 = new FileWriter("C:\\CancellationReport\\R1.txt",true);
    			f2.write(myFile_1+" updated"+"\n");
    			f2.close();
    			myWriter.close();
//    			Room cb = new Room();
//                cb.deleteFile(da2,ms4);
    			
                
            } catch (Exception e) {//if an exception occurs
            	JOptionPane.showMessageDialog(null, "Error occured while attempting to write to file: RoomCdetails-1" ,"Error Message",JOptionPane.ERROR_MESSAGE);	
            }
            File myFile_7=new File("..\\..\\Hotel\\BookingDetails\\"+ms4+"\\"+da2+".txt");
			//if (myFile_1.exists()) {
			try {
				Scanner myReader = new Scanner(myFile_7);
				while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				//System.out.println(data.length());
				if (data.length() == 0) {
		        	myFile_7.deleteOnExit();
		        	
		        	FileWriter f2 = new FileWriter("C:\\CancellationReport\\R1.txt",true);
	    			f2.write(myFile_1+" deleted"+"\n");
	    			f2.close();
				}
				}
				myReader.close();
				} catch (FileNotFoundException e) {
				//e.printStackTrace();
				}
            
    	}
        if(ms4=="RoomCdetails") {
        	String s=String.valueOf(a);
        	String s1=String.valueOf(b);
            String lineToEdit6 = (s1);//read the line to edit
            String replacementText6 = (s);//read the line to replace
            

            int startIndex6 = stringBufferOfData6.indexOf(lineToEdit6);
            int endIndex6 = startIndex6 + lineToEdit6.length();
           
            
            stringBufferOfData6.replace(startIndex6, endIndex6, replacementText6);
            
            try {
    			FileWriter myWriter6 = new FileWriter(myFile_6);
    			myWriter6.write(stringBufferOfData6.toString());
    			
    			FileWriter f3 = new FileWriter("C:\\CancellationReport\\R2.txt",true);
    			f3.write(myFile_6+" updated"+"\n");
    			f3.close();

    			myWriter6.close();
            } catch (Exception e) {//if an exception occurs
            	JOptionPane.showMessageDialog(null, "Error occured while attempting to write to file: RoomCdetails-2" ,"Error Message",JOptionPane.ERROR_MESSAGE);	
            }
    	}

        
	}
//========================================================================================================================================================================================================================    
    void runProcess1(int ms1,int ms2,LocalDate a,LocalDate b,String c,String ms4) {
    	
  		Scanner fileToRead1 = null;
  		stringBufferOfData1.delete(0, stringBufferOfData1.length());
		try {

				myFile_3=new File("..\\..\\Hotel\\C_Unbooking\\"+ms2+"_"+c+".txt");
				
				fileToRead1 = new Scanner(myFile_3); //point the scanner method to a file
				
	            for (String line1; fileToRead1.hasNextLine() && (line1 = fileToRead1.nextLine()) != null; ) {
	                
	                stringBufferOfData1.append(line1).append("\r\n");
	            }
	            	  
	            Room ca = new Room();
                ca.replacement1(ms1,a,b);  
	         
	            fileToRead1.close();
	                
        } catch (FileNotFoundException ex) {
            
            JOptionPane.showMessageDialog(null, "The file " + myFile_3 + " could not be found! " ,"Error Message",JOptionPane.ERROR_MESSAGE);

        } finally {
        	

            		
//        	if (myFile_1.exists()) {
//                
//                if (line.length() == 0) {
//                	//myFile_1.delete();
//                	myFile_1.deleteOnExit();
//                    System.out.println("EMPTY");
//                } else {
//                    System.out.println("NOT EMPTY");
//                }
//            } else {
//                System.out.println("DOES NOT EXISTS");
//            }
      

        	
            fileToRead1.close();
            
        }  
		

  	}   
	   


	void writeToFile1() {
	
    }
		
    void replacement1(int ms1,LocalDate a,LocalDate b)  {
    	LocalDate aa=a.minusDays(1);
		String s1=String.valueOf(aa);
		String s2=String.valueOf(b);
        String lineToEdit1      = s2;//read the line to edit
        String replacementText1 = s1;//read the line to replace
        
        int startIndex1 = stringBufferOfData1.indexOf(lineToEdit1);
        int endIndex1   = startIndex1 + lineToEdit1.length();
        
        stringBufferOfData1.replace(startIndex1, endIndex1, replacementText1);
        
        try {
			FileWriter myWriter = new FileWriter(myFile_3);
			myWriter.write(stringBufferOfData1.toString());
			
			FileWriter f4 = new FileWriter("C:\\CancellationReport\\R3.txt",true);
			f4.write(myFile_3+" updated"+"\n");
			f4.close();
			
			myWriter.close();
        } catch (Exception e) {//if an exception occurs
            JOptionPane.showMessageDialog(null, "Error occured while attempting to write to file: rp1" ,"Error Message",JOptionPane.ERROR_MESSAGE);
        }
       
	}
//========================================================================================================================================================================================================
    void runProcess2(int ms1,LocalDate da2,String ms4) throws IOException {
    	
  		Scanner fileToRead2 = null;
  		stringBufferOfData2.delete(0, stringBufferOfData2.length());
		try {
				myFile_4=new File("..\\..\\Hotel\\Unbooked\\"+ms4+"\\"+da2+".txt");
				fileToRead2 = new Scanner(myFile_4); //point the scanner method to a file

	            for (String line2; fileToRead2.hasNextLine() && (line2 = fileToRead2.nextLine()) != null; ) {
	                stringBufferOfData2.append(line2).append("\r\n");   
	            }
	            try {
	            	String ss=String.valueOf(stringBufferOfData2);
		            String[] NumArray = ss.split(",");
		            int n = NumArray.length; 
		            
		            Integer[] ar = new Integer[n];
		            for(int k=0;k<n-1;k++) {
		            	int lt =Integer.parseInt(NumArray[k]);
		            	ar[k] = lt;	
		            }
		            ar[n-1] = ms1;
	        		Arrays.sort(ar);
	        		
	        		String A = "";
	        		for (int p =0 ; p<ar.length;p++) {
						String q = String.valueOf(ar[p])+",";
						A += q;	
					}
	        		
		            Room cc = new Room();
	                cc.replacement2(ss,A,n,da2,ms4);
		            
		            fileToRead2.close();
	            
	            }catch(StringIndexOutOfBoundsException e) {
//           	JOptionPane.showMessageDialog(null, "" ,"Error Message",JOptionPane.ERROR_MESSAGE);
	            }		    
        } catch (FileNotFoundException ex) {//if the file cannot be found 
            JOptionPane.showMessageDialog(null, "The file Pr3 could not be found! " ,"Error Message",JOptionPane.ERROR_MESSAGE);
        } finally {
            fileToRead2.close();
            
        }  
    }	
//		void deleteFile2(LocalDate da2, String ms4) throws IOException {
//			File myFile_8=new File("..\\..\\Hotel\\Unbooked\\"+ms4+"\\"+da2+".txt");
//			try {
//				Scanner myReader = new Scanner(myFile_8);
//				while (myReader.hasNextLine()) {
//				String data = myReader.nextLine();
//				//System.out.println(data.length());
//				if (data.length() >= 21) {
//		        	myFile_8.deleteOnExit();
//		        	FileWriter f5 = new FileWriter("C:\\CancellationReport\\R4.txt",true);
//					f5.write(myFile_4+" deleted"+"\n");
//					f5.close();
//				}
//				}
//				myReader.close();
//				} catch (FileNotFoundException e) {
//				//e.printStackTrace();
//				}
//		 }
//				
			


	void replacement2(String ss,String A, int n, LocalDate da2, String ms4) throws IOException  {
		
		String lineToEdit2      = ss;//read the line to edit
        String replacementText2 = A;//read the line to replace
        
        int startIndex2 = stringBufferOfData2.indexOf(lineToEdit2);
        int endIndex2   = startIndex2 + lineToEdit2.length();
        
        stringBufferOfData2.replace(startIndex2, endIndex2, replacementText2);
         
        try {
			FileWriter myWriter2 = new FileWriter(myFile_4);
			myWriter2.write(stringBufferOfData2.toString());
			
			FileWriter f5 = new FileWriter("C:\\CancellationReport\\R4.txt",true);
			f5.write(myFile_4+" updated"+"\n");
			f5.close();
			
			myWriter2.close();
//			Room cb = new Room();
//            cb.deleteFile2(da2,ms4);
			
			//JOptionPane.showMessageDialog(null, "Booking cancellation successful " ,"Error Message",JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {//if an exception occurs
            JOptionPane.showMessageDialog(null, "Error occured while attempting to write to file rp2: " ,"Error Message",JOptionPane.ERROR_MESSAGE);
        }
        File myFile_8=new File("..\\..\\Hotel\\Unbooked\\"+ms4+"\\"+da2+".txt");
		try {
			Scanner myReader = new Scanner(myFile_8);
			while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			//System.out.println(data.length());
			if (data.length() >= 21) {
				
	        	myFile_8.deleteOnExit();
	        	FileWriter f5 = new FileWriter("C:\\CancellationReport\\R4.txt",true);
				f5.write(myFile_4+" deleted"+"\n");
				f5.close();
			}
			}
			myReader.close();
			} catch (FileNotFoundException e) {
			//e.printStackTrace();
			}
	}

  	   
	   
//=========================================================================================================================================================================================================    
}

class LuxurySingleRoom extends Room
{
	public void CancelProcess3()
	{
		
	}
}


class DeluxSingleRoom extends Room
{ 
	public void CancelProcess4()
	{
		
	}
}


class LuxuryDoubleRoom extends Room
{
	public void CancelProcess5()
	{
		
	}
}


class DeluxDoubleRoom extends Room
{
	public void CancelProcess6()
	{
		
	}
}

