package HotelManagement;

import java.awt.EventQueue;

import javax.swing.JFrame;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import HotelManagement.Management;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JTextPane;
import java.awt.Button;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import java.awt.Window.Type;
import javax.swing.border.LineBorder;
import javax.swing.JList;
import javax.swing.JComboBox;
import com.toedter.calendar.JDayChooser;

public class Management {

	private JFrame frmHotelManagementSystemmanager;
	private JTextField textFieldRoom;
	JDateChooser dateChooser;
	JDateChooser dateChooser_1;
	JDateChooser dateChooser_2;
	int retrive;
	JTextPane textPane;
	JRadioButton rdbtnDD;
	JRadioButton rdbtnLD;
	JRadioButton rdbtnDS;
	JRadioButton rdbtnLS;
	JRadioButton rdbtnUnbooked;
	JRadioButton rdbtnBooked;
	JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JTextField textFieldRoomNo;
	JRadioButton rdbtnDD2;
	JRadioButton rdbtnLD2;
	JRadioButton rdbtnDS2;
	JRadioButton rdbtnLS2;
	String roomText;
	String[] tempsArray;
	String[] mytempsArray;
	List<String> temps;
	List<String> temps2;
	static String gdToday;
	JButton btnNewButton_3;
	JButton btnNewButton_3_1_1;
	List<String> mytemps;
	
	
	public String mygdToday() {
		SimpleDateFormat today=new SimpleDateFormat("yyyy-MM-dd");
		Date date=new Date();
		gdToday=today.format(date);
		return gdToday;
	}
	
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Management window = new Management();
					window.frmHotelManagementSystemmanager.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Management() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	public void unBooked() {
		
		String gd=null;
		
		try {
			
			Date givenDate=dateChooser_2.getDate();
			DateFormat myFormat=new SimpleDateFormat("yyyy-MM-dd");
			gd=myFormat.format(givenDate);
			
			switch(retrive) {
			
			case 1:
				try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Delux Double Room\\"+gd+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Unbooked Delux Double Rooms on "+gd+" are:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Delux Double Rooms are Available on "+gd+"\n\n");
				}
				break;
				
			case 2:
				try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Luxury Double Room\\"+gd+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Unbooked Luxury Double Rooms on "+gd+" are:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Luxury Double Rooms are Available on "+gd+"\n\n");
				}
				break;
				
			case 3:
				try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Delux Single Room\\"+gd+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Unbooked Delux Single Rooms on "+gd+" are:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Delux Single Rooms are Available on "+gd+"\n\n");
				}
				break;
				
			case 4:
				try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Luxury Single Room\\"+gd+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Unbooked Luxury Single Rooms on "+gd+" are:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Luxury Single Rooms are Available on "+gd+"\n\n");
				}
				break;
			
			}
			
		}catch(Exception ex) {
			JOptionPane.showMessageDialog(dateChooser, "Enter a Correct Date!","Error Message",JOptionPane.ERROR_MESSAGE);
		}
		
		
		
	}
	
	
	
	
	private void initialize() {
		frmHotelManagementSystemmanager = new JFrame();
		frmHotelManagementSystemmanager.setResizable(false);
		frmHotelManagementSystemmanager.setTitle("Hotel Management System_Manager View");
		frmHotelManagementSystemmanager.setBounds(100, 100, 1183, 695);
		frmHotelManagementSystemmanager.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHotelManagementSystemmanager.getContentPane().setLayout(null);
		
		dateChooser = new JDateChooser();
		dateChooser.setBounds(281, 169, 147, 20);
		frmHotelManagementSystemmanager.getContentPane().add(dateChooser);
		
		final JButton btnSearchByDate = new JButton("Search");
		btnSearchByDate.setBackground(new Color(255, 182, 193));
		btnSearchByDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				rdbtnUnbooked.setSelected(false);
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				rdbtnBooked.setSelected(false);
				//dateChooser.setDate(null);
				dateChooser_1.setDate(null);
				dateChooser_2.setDate(null);
				textFieldRoom.setText(null);
				textPane.setText(null);
				rdbtnDD.setEnabled(false);
				rdbtnLD.setEnabled(false);
				rdbtnDS.setEnabled(false);
				rdbtnLS.setEnabled(false);
				dateChooser_2.setEnabled(false);
				
				try {
					
					Date givenDate=dateChooser.getDate();	
					DateFormat myFormat=new SimpleDateFormat("yyyy-MM-dd");
					String gd=myFormat.format(givenDate);
					
					try {
						
						File myFile=new File("..\\..\\Hotel\\BookingDetails\\AllRooms\\"+gd+".txt");
						Scanner myScanner=new Scanner(myFile);
						while(myScanner.hasNextLine()) {
							textPane.setText("Rooms Booked on "+gd+" are :\n"+myScanner.nextLine());
						}
						
						
					}catch(Exception ex) {
						textPane.setText("No Rooms Are Booked on "+gd);
					}
					
					try {
						
						File myFile=new File("..\\..\\Hotel\\BookingDetails\\Delux Double Room\\"+gd+".txt");
						Scanner myScanner=new Scanner(myFile);
						while(myScanner.hasNextLine()) {
							textPane.setText(textPane.getText()+"\n\nDelux Double Rooms:\n"+myScanner.nextLine());
						}
						
					}catch(Exception ex) {
						textPane.setText(textPane.getText()+"\n\nNo Delux Double Rooms Booked");
					}
					
					try {
						
						File myFile=new File("..\\..\\Hotel\\BookingDetails\\Luxury Double Room\\"+gd+".txt");
						Scanner myScanner=new Scanner(myFile);
						while(myScanner.hasNextLine()) {
							textPane.setText(textPane.getText()+"\n\nLuxury Double Rooms:\n"+myScanner.nextLine());
						}
						
					}catch(Exception ex) {
						textPane.setText(textPane.getText()+"\n\nNo Luxury Double Rooms Booked");
					}
					
					try {
						
						File myFile=new File("..\\..\\Hotel\\BookingDetails\\Delux Single Room\\"+gd+".txt");
						Scanner myScanner=new Scanner(myFile);
						while(myScanner.hasNextLine()) {
							textPane.setText(textPane.getText()+"\n\nDelux Single Rooms:\n"+myScanner.nextLine());
						}
						
					}catch(Exception ex) {
						textPane.setText(textPane.getText()+"\n\nNo Delux Single Rooms Booked");
					}
					
					try {
						
						File myFile=new File("..\\..\\Hotel\\BookingDetails\\Luxury Single Room\\"+gd+".txt");
						Scanner myScanner=new Scanner(myFile);
						while(myScanner.hasNextLine()) {
							textPane.setText(textPane.getText()+"\n\nLuxury Single Rooms:\n "+myScanner.nextLine());
						}
						
					}catch(Exception ex) {
						textPane.setText(textPane.getText()+"\n\nNo Luxury SIngle Romms Booked");
					}
					
				}catch(Exception ex) {
					JOptionPane.showMessageDialog(btnSearchByDate, "Enter a Correct Date","Error Message",JOptionPane.ERROR_MESSAGE);
				}
				
				
				
			}
		});
		btnSearchByDate.setBounds(280, 194, 123, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnSearchByDate);
		
		JLabel lblNewLabel = new JLabel("Search by Date");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(281, 139, 127, 23);
		frmHotelManagementSystemmanager.getContentPane().add(lblNewLabel);
		
		JLabel lblSearchByRoom = new JLabel("Search by Room");
		lblSearchByRoom.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSearchByRoom.setBounds(282, 240, 127, 23);
		frmHotelManagementSystemmanager.getContentPane().add(lblSearchByRoom);
		
		textFieldRoom = new JTextField();
		textFieldRoom.setBounds(282, 270, 147, 20);
		frmHotelManagementSystemmanager.getContentPane().add(textFieldRoom);
		textFieldRoom.setColumns(10);
		
		dateChooser_1 = new JDateChooser();
		dateChooser_1.setBounds(282, 295, 147, 20);
		frmHotelManagementSystemmanager.getContentPane().add(dateChooser_1);
		
		final JButton btnNewButton = new JButton("Search");
		btnNewButton.setBackground(new Color(255, 182, 193));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				rdbtnUnbooked.setSelected(false);
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				rdbtnBooked.setSelected(false);
				dateChooser.setDate(null);
				//dateChooser_1.setDate(null);
				dateChooser_2.setDate(null);
				//textFieldRoom.setText(null);
				textPane.setText(null);
				rdbtnDD.setEnabled(false);
				rdbtnLD.setEnabled(false);
				rdbtnDS.setEnabled(false);
				rdbtnLS.setEnabled(false);
				dateChooser_2.setEnabled(false);
				
				String roomNo = null;
				String gd = null;
				
				
				textPane.setText("");
				
				try {
					
					roomNo=textFieldRoom.getText();
					int x=Integer.parseInt(roomNo);
					
					
					
				}catch(Exception ex) {
					JOptionPane.showMessageDialog(btnNewButton, "Enter a Correct Room Number!","Error Message",JOptionPane.ERROR_MESSAGE);
				}
				
				try {
					
					Date givenDate=dateChooser_1.getDate();
					DateFormat myFormat=new SimpleDateFormat("yyyy-MM-dd");
					gd=myFormat.format(givenDate);
					
				}catch(Exception ex) {
					JOptionPane.showMessageDialog(btnNewButton, "Enter a Correct Date!","Error Message",JOptionPane.ERROR_MESSAGE);
				}
				
				try {
					
					if(0<Integer.valueOf(roomNo) && Integer.valueOf(roomNo)<40){

						try {
							
							File myFile=new File("..\\..\\Hotel\\BookingDetails\\RoomCdetails\\"+roomNo+"_"+gd+".txt");
							
							Scanner myScanner=new Scanner(myFile);
							while(myScanner.hasNextLine()) {
								textPane.setText(textPane.getText()+myScanner.nextLine()+"\n");
								
							}
							
						}catch(Exception ex) {
							
							if(roomNo!=null && gd!=null) {
								textPane.setText("Room "+roomNo+" is not Booked on "+gd);
							}else {
								textPane.setText("");
								//JOptionPane.showMessageDialog(btnNewButton, "Fill Both Date and Room Number","Error Message",JOptionPane.ERROR_MESSAGE);
							}
							
							
						}
				
					}else {
						JOptionPane.showMessageDialog(btnNewButton, "Invalid Room Number!","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
				}catch(Exception ex) {
					
				}
				
				
				
				
				
				
				
			}
		});
		btnNewButton.setBounds(282, 322, 123, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton);
		
		rdbtnBooked = new JRadioButton("Booked Rooms");
		rdbtnBooked.setBackground(new Color(255, 218, 185));
		rdbtnBooked.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				rdbtnUnbooked.setSelected(false);
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				rdbtnBooked.setSelected(true);
				dateChooser.setDate(null);
				dateChooser_1.setDate(null);
				dateChooser_2.setDate(null);
				textFieldRoom.setText(null);
				textPane.setText(null);
				rdbtnDD.setEnabled(false);
				rdbtnLD.setEnabled(false);
				rdbtnDS.setEnabled(false);
				rdbtnLS.setEnabled(false);
				dateChooser_2.setEnabled(false);
				
				
				textPane.setText("All Booked Rooms\n");
				File myFolder=new File("..\\..\\Hotel\\BookingDetails\\AllRooms\\");
				
				try {
					
                    for(File sourceFile:myFolder.listFiles()) {
						
						Scanner myScanner=new Scanner(sourceFile);
			
						String fileName=sourceFile.getName();
						StringBuilder sb=new StringBuilder(fileName);
						StringBuilder newFileName=sb.deleteCharAt(fileName.length()-3);
						StringBuilder newFileName_1=sb.deleteCharAt(newFileName.length()-2);
						StringBuilder newFileName_2=sb.deleteCharAt(newFileName_1.length()-1);
						
						
						textPane.setText(textPane.getText()+"\n"+newFileName_2+" : "+myScanner.nextLine());
						
						myScanner.close();
					}
					
				}catch(Exception ex) {
					
				}
				
			}
		});
		rdbtnBooked.setBounds(282, 97, 122, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnBooked);
		
		rdbtnUnbooked = new JRadioButton("Unbooked Rooms");
		rdbtnUnbooked.setBackground(new Color(255, 228, 196));
		rdbtnUnbooked.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnUnbooked.setSelected(true);
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				rdbtnBooked.setSelected(false);
				dateChooser.setDate(null);
				dateChooser_1.setDate(null);
				dateChooser_2.setDate(null);
				textFieldRoom.setText(null);
				textPane.setText(null);
				rdbtnDD.setEnabled(true);
				rdbtnLD.setEnabled(true);
				rdbtnDS.setEnabled(true);
				rdbtnLS.setEnabled(true);
				dateChooser_2.setEnabled(true);
			}
		});
		rdbtnUnbooked.setBounds(506, 97, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnUnbooked);
		
		JLabel lblBookedunbookedRooms = new JLabel("Booked/Unbooked Rooms");
		lblBookedunbookedRooms.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBookedunbookedRooms.setBounds(282, 72, 193, 23);
		frmHotelManagementSystemmanager.getContentPane().add(lblBookedunbookedRooms);
		
		dateChooser_2 = new JDateChooser();
		dateChooser_2.setBounds(525, 139, 147, 20);
		frmHotelManagementSystemmanager.getContentPane().add(dateChooser_2);
		dateChooser_2.setEnabled(false);
		
		rdbtnDD = new JRadioButton("Delux Double Rooms");
		rdbtnDD.setForeground(new Color(0, 0, 0));
		rdbtnDD.setBackground(new Color(135, 206, 250));
		rdbtnDD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnDD.setSelected(true);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				retrive=1;
			}
		});
		rdbtnDD.setBounds(525, 165, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnDD);
		rdbtnDD.setEnabled(false);
		
		rdbtnLD = new JRadioButton("Luxury Double Rooms");
		rdbtnLD.setBackground(new Color(135, 206, 235));
		rdbtnLD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(true);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				retrive=2;
				
			}
		});
		rdbtnLD.setBounds(525, 185, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnLD);
		rdbtnLD.setEnabled(false);
		
		rdbtnDS = new JRadioButton("Delux Single Rooms");
		rdbtnDS.setBackground(new Color(135, 206, 235));
		rdbtnDS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(true);
				rdbtnLS.setSelected(false);
				retrive=3;
			}
		});
		rdbtnDS.setBounds(525, 205, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnDS);
		rdbtnDS.setEnabled(false);
		
		rdbtnLS = new JRadioButton("Luxury Single Rooms");
		rdbtnLS.setBackground(new Color(135, 206, 235));
		rdbtnLS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				retrive=4;
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(true);
				
			}
		});
		rdbtnLS.setBounds(525, 225, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnLS);
		rdbtnLS.setEnabled(false);
		
		btnNewButton_1 = new JButton("Search");
		btnNewButton_1.setBackground(new Color(255, 182, 193));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtnDD.isSelected()||rdbtnLD.isSelected()||rdbtnDS.isSelected()||rdbtnLS.isSelected()) {
					unBooked();
				}else {
					JOptionPane.showMessageDialog(dateChooser, "Select a Room Type!", "Error Message", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		btnNewButton_1.setBounds(525, 257, 123, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_1 = new JLabel("Manager View");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNewLabel_1.setBounds(351, 20, 264, 36);
		frmHotelManagementSystemmanager.getContentPane().add(lblNewLabel_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 128, 128));
		panel.setBounds(269, 14, 425, 50);
		frmHotelManagementSystemmanager.getContentPane().add(panel);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 14, 249, 364);
		frmHotelManagementSystemmanager.getContentPane().add(scrollPane);
		
		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setBackground(new Color(192, 192, 192));
		
		JButton btnClear = new JButton("Clear All");
		btnClear.setBackground(new Color(255, 182, 193));
		btnClear.setBounds(525, 317, 123, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnClear);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				rdbtnUnbooked.setSelected(false);
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				rdbtnBooked.setSelected(false);
				dateChooser.setDate(null);
				dateChooser_1.setDate(null);
				dateChooser_2.setDate(null);
				textFieldRoom.setText(null);
				textPane.setText(null);
				rdbtnDD.setEnabled(false);
				rdbtnLD.setEnabled(false);
				rdbtnDS.setEnabled(false);
				rdbtnLS.setEnabled(false);
				dateChooser_2.setEnabled(false);
				
				
				
				
			}
		});
		
		
		
		
		btnNewButton_2 = new JButton("Available Today");
		btnNewButton_2.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_2.setBackground(new Color(255, 182, 193));
		btnNewButton_2.setBounds(525, 287, 123, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_2);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				rdbtnUnbooked.setSelected(false);
				rdbtnDD.setSelected(false);
				rdbtnLD.setSelected(false);
				rdbtnDS.setSelected(false);
				rdbtnLS.setSelected(false);
				rdbtnBooked.setSelected(false);
				dateChooser.setDate(null);
				dateChooser_1.setDate(null);
				dateChooser_2.setDate(null);
				textFieldRoom.setText(null);
				textPane.setText(null);
				rdbtnDD.setEnabled(false);
				rdbtnLD.setEnabled(false);
				rdbtnDS.setEnabled(false);
				rdbtnLS.setEnabled(false);
				dateChooser_2.setEnabled(false);
				
				
				mygdToday();
				/*SimpleDateFormat today=new SimpleDateFormat("yyyy-MM-dd");
				Date date=new Date();
				gdToday=today.format(date);*/
				textPane.setText("Date: "+gdToday+"\n\n");
				
				
				try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Delux Single Room\\"+gdToday+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Delux Single Rooms Available Today:\n "+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Delux Single Rooms Available Today"+"\n\n");
				}
				
				try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Luxury Single Room\\"+gdToday+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Luxury Single Rooms Available Today:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Luxury Single Rooms Available Today"+"\n\n");
				}
				
                try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Delux Double Room\\"+gdToday+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Delux Double Rooms Available Today:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Delux Double Rooms Available Today"+"\n\n");
				}
                
                try {
					
					File myFile=new File("..\\..\\Hotel\\Unbooked\\Luxury Double Room\\"+gdToday+".txt");
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane.setText(textPane.getText()+"Luxury Double Rooms Available Today:\n"+myScanner.nextLine()+"\n\n");
					}
					
				}catch(Exception ex) {
					textPane.setText(textPane.getText()+"All Luxury Double Rooms Available Today"+"\n\n");
				}
				
				
			}
		});
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(997, 78, 161, 170);
		frmHotelManagementSystemmanager.getContentPane().add(scrollPane_1);
		
		JTextPane textPane_1 = new JTextPane();
		scrollPane_1.setViewportView(textPane_1);
		
		JPanel panel_1_2 = new JPanel();
		panel_1_2.setToolTipText("");
		panel_1_2.setBackground(new Color(224, 255, 255));
		panel_1_2.setBounds(269, 72, 425, 306);
		frmHotelManagementSystemmanager.getContentPane().add(panel_1_2);
		
		JLabel lblNewLabel_1_1 = new JLabel("Maintenance");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNewLabel_1_1.setBounds(831, 20, 231, 36);
		frmHotelManagementSystemmanager.getContentPane().add(lblNewLabel_1_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 102, 153));
		panel_1.setBounds(704, 14, 463, 50);
		frmHotelManagementSystemmanager.getContentPane().add(panel_1);
		
		JComboBox<String> comboBox = new JComboBox<>();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				
			}
		});
		comboBox.setBounds(714, 268, 144, 22);
		frmHotelManagementSystemmanager.getContentPane().add(comboBox);
		
		
		rdbtnDD2 = new JRadioButton("Delux Double");
		rdbtnDD2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comboBox.removeAllItems();
				btnNewButton_3_1_1.setEnabled(false);
				btnNewButton_3.setEnabled(false);
				textFieldRoomNo.setEnabled(false);
				rdbtnDD2.setSelected(true);
				rdbtnLD2.setSelected(false);
				rdbtnDS2.setSelected(false);
				rdbtnLS2.setSelected(false);
				
				roomText="Delux Double Room";
				
				
				
			}
		});
		rdbtnDD2.setBackground(new Color(64, 224, 208));
		rdbtnDD2.setBounds(714, 79, 109, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnDD2);
		
		rdbtnLD2 = new JRadioButton("Luxury Double");
		rdbtnLD2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comboBox.removeAllItems();
				btnNewButton_3_1_1.setEnabled(false);
				btnNewButton_3.setEnabled(false);
				textFieldRoomNo.setEnabled(false);
				rdbtnDD2.setSelected(false);
				rdbtnLD2.setSelected(true);
				rdbtnDS2.setSelected(false);
				rdbtnLS2.setSelected(false);
				
				roomText="Luxury Double Room";
				
			}
		});
		rdbtnLD2.setBackground(new Color(64, 224, 208));
		rdbtnLD2.setBounds(714, 114, 109, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnLD2);
		
		rdbtnDS2 = new JRadioButton("Delux Single");
		rdbtnDS2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comboBox.removeAllItems();
				btnNewButton_3_1_1.setEnabled(false);
				btnNewButton_3.setEnabled(false);
				textFieldRoomNo.setEnabled(false);
				rdbtnDD2.setSelected(false);
				rdbtnLD2.setSelected(false);
				rdbtnDS2.setSelected(true);
				rdbtnLS2.setSelected(false);
				
				roomText="Delux Single Room";
				
			}
		});
		rdbtnDS2.setBackground(new Color(64, 224, 208));
		rdbtnDS2.setBounds(714, 149, 109, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnDS2);
		
		rdbtnLS2 = new JRadioButton("Luxury Single");
		rdbtnLS2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comboBox.removeAllItems();
				btnNewButton_3_1_1.setEnabled(false);
				btnNewButton_3.setEnabled(false);
				textFieldRoomNo.setEnabled(false);
				rdbtnDD2.setSelected(false);
				rdbtnLD2.setSelected(false);
				rdbtnDS2.setSelected(false);
				rdbtnLS2.setSelected(true);
				
				roomText="Luxury Single Room";
				
				
			}
		});
		rdbtnLS2.setBackground(new Color(64, 224, 208));
		rdbtnLS2.setBounds(714, 184, 109, 23);
		frmHotelManagementSystemmanager.getContentPane().add(rdbtnLS2);
		
		
		
		btnNewButton_3 = new JButton("Make Unavailable");
		btnNewButton_3.setEnabled(false);
		btnNewButton_3.setBackground(new Color(255, 182, 193));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textFieldRoomNo.setEnabled(false);
				btnNewButton_3_1_1.setEnabled(false);
				String comboValue=String.valueOf(comboBox.getSelectedItem());
				
				mygdToday();
				
				try {
					
					File myFile=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
					FileWriter myWriter=new FileWriter(myFile.getAbsoluteFile(),true);
					BufferedWriter myBuffer=new BufferedWriter(myWriter);
					myBuffer.write(comboValue);
					myBuffer.close();
					myWriter.close();
					
					
					File History=new File("..\\..\\Hotel\\Maintenance\\Maintenance History.txt");
					FileWriter myHistoryWriter=new FileWriter(History.getAbsoluteFile(),true);
					BufferedWriter myHistoryBuffer=new BufferedWriter(myHistoryWriter);
					myHistoryBuffer.write("Room "+comboValue+" Set Unavailable on "+gdToday+"\n");
					myHistoryBuffer.close();
					myHistoryWriter.close();
					
					
					
					File myFile2=new File("..\\..\\Hotel\\Unbooked\\"+roomText+"\\"+gdToday+".txt");
					
					
					
					String token1="";
					Scanner myScanner=new Scanner(myFile2).useDelimiter(",\\s*");
					
					temps=new ArrayList<String>();
					
					while(myScanner.hasNext()) {
						token1=myScanner.next();
						temps.add(token1+"\n");
					}
					
					myScanner.close();
					
					Scanner myScanner2=new Scanner(myFile);
					temps2=new ArrayList<String>();
					while(myScanner2.hasNext()) {
						temps2.add(myScanner2.next()+"\n");
					}
					
					
					textPane_1.setText("Want to make more Rooms Unavailable?\n\nAvailable "+roomText+"s on "+gdToday+"\n");
					
					temps.removeAll(temps2);
					tempsArray=temps.toArray(new String[0]);
					
					for(String s:tempsArray) {
						textPane_1.setText(textPane_1.getText()+s);
					}
					
					
					comboBox.removeItem(comboValue);					
				
				}catch(Exception ex) {
					
					
					
                    File myFileex=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\"+roomText+"_"+gdToday+".txt");
					
					
					
					String token1="";
					
					try {
						
						Scanner myScannerex=new Scanner(myFileex);
						
						mytemps=new ArrayList<String>();
						
						while(myScannerex.hasNext()) {
							token1=myScannerex.next();
							mytemps.add(token1+"\n");
						}
						
						myScannerex.close();
						
					}catch(Exception exa) {
						System.out.println(exa);
					}
					
					
					
					File myFileun=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
					
					try {
						
						Scanner myScannerun=new Scanner(myFileun);
						temps2=new ArrayList<String>();
						while(myScannerun.hasNext()) {
							temps2.add(myScannerun.next()+"\n");
						}
						
						
						textPane_1.setText("Want to make more Rooms Unavailable?\n\nAvailable "+roomText+"s on "+gdToday+"\n");
						
						/*System.out.println("Temps2:"+temps2);
						System.out.println("My T: "+mytemps);*/
						mytemps.removeAll(temps2);
						/*System.out.println(mytemps.removeAll(temps2));*/
						mytempsArray=mytemps.toArray(new String[0]);
						
						for(String s:mytempsArray) {
							textPane_1.setText(textPane_1.getText()+s);
						}
						
						
					}catch(Exception exb) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
						
					}
					
					
					
					
					comboBox.removeItem(comboValue);	
					
		
				}
				
				
				

				
				
				
			}
		});
		btnNewButton_3.setBounds(924, 268, 135, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_3_1 = new JButton("Unavailable Rooms");
		btnNewButton_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				mygdToday();
				comboBox.removeAllItems();
				btnNewButton_3.setEnabled(false);
				textFieldRoomNo.setEnabled(true);
				btnNewButton_3_1_1.setEnabled(true);
				
				File myFile=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
				textPane_1.setText("Currently Unavailable Rooms due to Maintenance on "+gdToday+"\n");
				
				try {
					
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane_1.setText(textPane_1.getText()+myScanner.nextLine()+"\n");
					}
					
				}catch(Exception ex) {
					JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
				}
				
				
			}
		});
		btnNewButton_3_1.setBackground(new Color(255, 182, 193));
		btnNewButton_3_1.setBounds(837, 116, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_3_1);
		
		JLabel lblFromDate_1_1 = new JLabel("Make Room Available");
		lblFromDate_1_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFromDate_1_1.setBounds(714, 289, 166, 23);
		frmHotelManagementSystemmanager.getContentPane().add(lblFromDate_1_1);
		
		textFieldRoomNo = new JTextField();
		textFieldRoomNo.setEnabled(false);
		textFieldRoomNo.setBounds(714, 336, 144, 20);
		frmHotelManagementSystemmanager.getContentPane().add(textFieldRoomNo);
		textFieldRoomNo.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Enter Room No:");
		lblNewLabel_2.setBounds(714, 319, 144, 14);
		frmHotelManagementSystemmanager.getContentPane().add(lblNewLabel_2);
		
		
		
		JButton btnNewButton_3_1_2 = new JButton("Available Rooms");
		btnNewButton_3_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(roomText!=null) {
					btnNewButton_3.setEnabled(true);
				}
				
				
				btnNewButton_3_1_1.setEnabled(false);
				mygdToday();
				String gd = null;
				//System.out.println("today: "+gdToday);
				textPane_1.setText("");
				
				File myFile0=new File("..\\..\\Hotel\\Maintenance");
				myFile0.mkdirs();
				
				File myFileNIUU=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable");
				myFileNIUU.mkdirs();
				
				File myFileAR=new File("..\\..\\Hotel\\Maintenance\\All Rooms");
				myFileAR.mkdirs();
					
				textFieldRoomNo.setEnabled(false);
					
					try {
						
						File myFile=new File("..\\..\\Hotel\\Unbooked\\"+roomText+"\\"+gdToday+".txt");
						
						
							
							String token1="";
							Scanner myScanner=new Scanner(myFile).useDelimiter(",\\s*");
							
							temps=new ArrayList<String>();
							
							while(myScanner.hasNext()) {
								token1=myScanner.next();
								temps.add(token1+"\n");
							}
							
							myScanner.close();
							
							
							
							File myFile2=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
							if(myFile2.exists()==false) {
								myFile2.createNewFile();
							}
							
							
							Scanner myScanner2=new Scanner(myFile2);
							temps2=new ArrayList<String>();
							while(myScanner2.hasNext()) {
								temps2.add(myScanner2.next()+"\n");
							}
							
							/*System.out.println(temps);
							System.out.println(temps2);
							System.out.println(temps2.size());*/
							temps.removeAll(temps2);
							//System.out.println(temps.removeAll(temps2));
							
							textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
							
							
							tempsArray=temps.toArray(new String[0]);
							
							for(String s:tempsArray) {
								textPane_1.setText(textPane_1.getText()+s);
							}
							
							comboBox.setModel(new DefaultComboBoxModel<>(tempsArray));
							comboBox.setVisible(true);
							
							

					
				}catch(Exception ex) {
					
					if(roomText==null) {
						JOptionPane.showMessageDialog(btnNewButton_3_1_2, "Select a Room Type","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
					
					
					
					if(roomText=="Delux Double Room") {
						
						File myFileCreate=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\"+roomText+"_"+gdToday+".txt");
						
						if(myFileCreate.exists()==false) {
							
							try {
								FileWriter createWriter=new FileWriter(myFileCreate.getAbsoluteFile(),true);
								BufferedWriter createBufferedWriter=new BufferedWriter(createWriter);
								
								String[] createArray= {"31","32","33","34","35","36","37","38","39","40"};
								for(String s:createArray) {
									createBufferedWriter.write(s+"\n");
								}
								
								createBufferedWriter.close();
								createWriter.close();
								
								textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
								Scanner createScanner=new Scanner(myFileCreate);
								while(createScanner.hasNextLine()) {
									textPane_1.setText(textPane_1.getText()+createScanner.nextLine()+"\n");
								}
								
								
								
								
								comboBox.setModel(new DefaultComboBoxModel<>(createArray));
								comboBox.setVisible(true);
								
								
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
							
						}else {
							try {
								
                                textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
									
								Scanner retriveScanner=new Scanner(myFileCreate);
								List<String> retriveArray=new ArrayList<String>();
								while (retriveScanner.hasNextLine()){
									retriveArray.add(retriveScanner.nextLine()+"\n");
								}
								//System.out.println("AA"+retriveArray);
								
								File myFile2=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
								if(myFile2.exists()==false) {
									myFile2.createNewFile();
								}
								
								
								Scanner myScanner2=new Scanner(myFile2);
								temps2=new ArrayList<String>();
								while(myScanner2.hasNext()) {
									temps2.add(myScanner2.next()+"\n");
								}
								
								
								/*System.out.println(temps2);
								System.out.println(temps2.size());*/
								retriveArray.removeAll(temps2);
								
								
								tempsArray=retriveArray.toArray(new String[0]);
								//System.out.println("TA:"+tempsArray);
								
								for(String s:tempsArray) {
									textPane_1.setText(textPane_1.getText()+s);
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(tempsArray));
								comboBox.setVisible(true);
								
								
								
								
								
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
						}
						
						
						
						
					} else if(roomText=="Luxury Double Room") {
						
						File myFileCreate=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\"+roomText+"_"+gdToday+".txt");
						
						if(myFileCreate.exists()==false) {
							
							try {
								FileWriter createWriter=new FileWriter(myFileCreate.getAbsoluteFile(),true);
								BufferedWriter createBufferedWriter=new BufferedWriter(createWriter);
								
								String[] createArray= {"21","22","23","24","25","26","27","28","29","30"};
								
								for(String s:createArray) {
									createBufferedWriter.write(s+"\n");
								}
								
								createBufferedWriter.close();
								createWriter.close();
								
								textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
								Scanner createScanner=new Scanner(myFileCreate);
								while(createScanner.hasNextLine()) {
									textPane_1.setText(textPane_1.getText()+createScanner.nextLine()+"\n");
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(createArray));
								comboBox.setVisible(true);
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
							
						}else {
							try {
								
                                textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
                                Scanner retriveScanner=new Scanner(myFileCreate);
								List<String> retriveArray=new ArrayList<String>();
								while (retriveScanner.hasNextLine()){
									retriveArray.add(retriveScanner.nextLine()+"\n");
								}
								//System.out.println("AA"+retriveArray);
								
								File myFile2=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
								if(myFile2.exists()==false) {
									myFile2.createNewFile();
								}
								
								
								Scanner myScanner2=new Scanner(myFile2);
								temps2=new ArrayList<String>();
								while(myScanner2.hasNext()) {
									temps2.add(myScanner2.next()+"\n");
								}
								
								
								/*System.out.println(temps2);
								System.out.println(temps2.size());*/
								retriveArray.removeAll(temps2);
								
								
								tempsArray=retriveArray.toArray(new String[0]);
								//System.out.println("TA:"+tempsArray);
								
								for(String s:tempsArray) {
									textPane_1.setText(textPane_1.getText()+s);
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(tempsArray));
								comboBox.setVisible(true);
								
								
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
						}
						
						
						
						
					}else if(roomText=="Delux Single Room") {
						
						File myFileCreate=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\"+roomText+"_"+gdToday+".txt");
						
						if(myFileCreate.exists()==false) {
							
							try {
								FileWriter createWriter=new FileWriter(myFileCreate.getAbsoluteFile(),true);
								BufferedWriter createBufferedWriter=new BufferedWriter(createWriter);
								
								String[] createArray= {"11","12","13","14","15","16","17","18","19","20"};
								
								for(String s:createArray) {
									createBufferedWriter.write(s+"\n");
								}
								
								createBufferedWriter.close();
								createWriter.close();
								
								textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
								Scanner createScanner=new Scanner(myFileCreate);
								while(createScanner.hasNextLine()) {
									textPane_1.setText(textPane_1.getText()+createScanner.nextLine()+"\n");
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(createArray));
								comboBox.setVisible(true);
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
							
						}else {
							try {
								
                                textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
                                Scanner retriveScanner=new Scanner(myFileCreate);
								List<String> retriveArray=new ArrayList<String>();
								while (retriveScanner.hasNextLine()){
									retriveArray.add(retriveScanner.nextLine()+"\n");
								}
								//System.out.println("AA"+retriveArray);
								
								File myFile2=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
								if(myFile2.exists()==false) {
									myFile2.createNewFile();
								}
								
								
								Scanner myScanner2=new Scanner(myFile2);
								temps2=new ArrayList<String>();
								while(myScanner2.hasNext()) {
									temps2.add(myScanner2.next()+"\n");
								}
								
								
								/*System.out.println(temps2);
								System.out.println(temps2.size());*/
								retriveArray.removeAll(temps2);
								
								
								tempsArray=retriveArray.toArray(new String[0]);
								//System.out.println("TA:"+tempsArray);
								
								for(String s:tempsArray) {
									textPane_1.setText(textPane_1.getText()+s);
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(tempsArray));
								comboBox.setVisible(true);
								
								
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
						}
						
						
						
						
					}else if(roomText=="Luxury Single Room") {
						
						File myFileCreate=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\"+roomText+"_"+gdToday+".txt");
						
						if(myFileCreate.exists()==false) {
							
							try {
								FileWriter createWriter=new FileWriter(myFileCreate.getAbsoluteFile(),true);
								BufferedWriter createBufferedWriter=new BufferedWriter(createWriter);
								
								String[] createArray= {"1","2","3","4","5","6","7","8","9","10"};
								
								for(String s:createArray) {
									createBufferedWriter.write(s+"\n");
								}
								
								createBufferedWriter.close();
								createWriter.close();
								
								textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
								Scanner createScanner=new Scanner(myFileCreate);
								while(createScanner.hasNextLine()) {
									textPane_1.setText(textPane_1.getText()+createScanner.nextLine()+"\n");
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(createArray));
								comboBox.setVisible(true);
								
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
							
						}else {
							try {
								
                                textPane_1.setText("Available "+roomText+"s on "+gdToday+"\n");
								
                                Scanner retriveScanner=new Scanner(myFileCreate);
								List<String> retriveArray=new ArrayList<String>();
								while (retriveScanner.hasNextLine()){
									retriveArray.add(retriveScanner.nextLine()+"\n");
								}
								//System.out.println("AA"+retriveArray);
								
								File myFile2=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
								if(myFile2.exists()==false) {
									myFile2.createNewFile();
								}
								
								
								Scanner myScanner2=new Scanner(myFile2);
								temps2=new ArrayList<String>();
								while(myScanner2.hasNext()) {
									temps2.add(myScanner2.next()+"\n");
								}
								
								
								/*System.out.println(temps2);
								System.out.println(temps2.size());*/
								retriveArray.removeAll(temps2);
								
								
								tempsArray=retriveArray.toArray(new String[0]);
								//System.out.println("TA:"+tempsArray);
								
								for(String s:tempsArray) {
									textPane_1.setText(textPane_1.getText()+s);
								}
								
								comboBox.setModel(new DefaultComboBoxModel<>(tempsArray));
								comboBox.setVisible(true);
								
								
								
							}catch(Exception exx) {
								JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							}
						}
						
						
						
						
					}
					
					
				
				}
				
				

				
			}
		});
		btnNewButton_3_1_2.setBackground(new Color(255, 182, 193));
		btnNewButton_3_1_2.setBounds(837, 86, 147, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_3_1_2);
		
		btnNewButton_3_1_1 = new JButton("Make Available");
		btnNewButton_3_1_1.setEnabled(false);
		btnNewButton_3_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				mygdToday();
				btnNewButton_3.setEnabled(false);
				textFieldRoomNo.setEnabled(true);
				btnNewButton_3_1_1.setEnabled(true);
				
				
						
						File myFile=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
						
						try {
							
							Scanner myScanner=new Scanner(myFile);
							temps2=new ArrayList<String>();
							while(myScanner.hasNext()) {
								temps2.add(myScanner.next()+"\n");
							}
							//System.out.println(temps2);
							
							
							
							String givenRoom=textFieldRoomNo.getText();
							
							
							File History=new File("..\\..\\Hotel\\Maintenance\\Maintenance History.txt");
							FileWriter myHistoryWriter=new FileWriter(History.getAbsoluteFile(),true);
							BufferedWriter myHistoryBuffer=new BufferedWriter(myHistoryWriter);
							myHistoryBuffer.write("Room "+givenRoom+"\n made available again on "+gdToday+"\n");
							myHistoryBuffer.close();
							myHistoryWriter.close();
							
							
							List<String> makeAvRoom=new ArrayList<String>();
							makeAvRoom.add(0, givenRoom+"\n");
							
							
							if(!temps2.contains(givenRoom+"\n")) {
								JOptionPane.showMessageDialog(btnNewButton_3_1_2, "Error Message");
							}
								
							
							
							/*System.out.println(makeAvRoom);
							System.out.println("First: "+temps2);*/
							temps2.removeAll(makeAvRoom);
							List<String> noDuplicates=new ArrayList<>(new HashSet<>(temps2));
							//System.out.println("Second: "+temps2);
							
							textPane_1.setText("Want to make more Rooms Available?\n\nCurrently Unavailable Rooms due to Maintenance on "+gdToday+"\n");
							for(String s:noDuplicates) {
								textPane_1.setText(textPane_1.getText()+s);
							}
							
							FileWriter delWriter=new FileWriter(myFile,false);
							delWriter.close();
							FileWriter myWriter=new FileWriter(myFile.getAbsoluteFile(),true);
							
							for(String s:temps2) {
								myWriter.write(s);
							}
							
							myWriter.close();
							
							comboBox.removeAllItems();
							rdbtnDD2.setSelected(false);
							rdbtnLD2.setSelected(false);
							rdbtnDS2.setSelected(false);
							rdbtnLS2.setSelected(false);
							textFieldRoomNo.setText("");
							
							
							
						}catch(Exception ex) {
							JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
							
						}
						
						

				
			}
		});
		btnNewButton_3_1_1.setBackground(new Color(255, 182, 193));
		btnNewButton_3_1_1.setBounds(924, 336, 135, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_3_1_1);
		
		JLabel lblFromDate_1_2 = new JLabel("Make Room Unavailable");
		lblFromDate_1_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFromDate_1_2.setBounds(717, 223, 193, 23);
		frmHotelManagementSystemmanager.getContentPane().add(lblFromDate_1_2);
		
		
		
		JButton btnNewButton_3_1_3 = new JButton("History");
		btnNewButton_3_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				btnNewButton_3_1_1.setEnabled(false);
				textFieldRoomNo.setEnabled(false);
				btnNewButton_3.setEnabled(false);
				comboBox.removeAllItems();
				textPane_1.setText("");
				
				File myFile=new File("..\\..\\Hotel\\Maintenance\\Maintenance History.txt");
				
				try {
					Scanner myScanner=new Scanner(myFile);
					while(myScanner.hasNextLine()) {
						textPane_1.setText(textPane_1.getText()+myScanner.nextLine()+"\n");
					}
					myScanner.close();
				} catch (FileNotFoundException e1) {
					
					JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		btnNewButton_3_1_3.setBackground(new Color(255, 182, 193));
		btnNewButton_3_1_3.setBounds(836, 147, 148, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_3_1_3);
		
		JButton btnNewButton_3_1_3_1 = new JButton("Clear All");
		btnNewButton_3_1_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				btnNewButton_3_1_1.setEnabled(false);
				textFieldRoomNo.setEnabled(false);
				btnNewButton_3.setEnabled(false);
				rdbtnDD2.setSelected(false);
				rdbtnLD2.setSelected(false);
				rdbtnDS2.setSelected(false);
				rdbtnLS2.setSelected(false);
				textPane_1.setText("");
				textFieldRoomNo.setText("");
				comboBox.removeAllItems();
				roomText=null;
				

				
			}
		});
		btnNewButton_3_1_3_1.setBackground(new Color(255, 182, 193));
		btnNewButton_3_1_3_1.setBounds(836, 178, 148, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnNewButton_3_1_3_1);
		
		JLabel lblNewLabel_2_1 = new JLabel("Select Room No:");
		lblNewLabel_2_1.setBounds(714, 251, 144, 14);
		frmHotelManagementSystemmanager.getContentPane().add(lblNewLabel_2_1);
		
		JPanel panel_1_2_1 = new JPanel();
		panel_1_2_1.setToolTipText("");
		panel_1_2_1.setBackground(new Color(175, 238, 238));
		panel_1_2_1.setBounds(703, 72, 464, 306);
		frmHotelManagementSystemmanager.getContentPane().add(panel_1_2_1);
		
		JButton btnSignOut = new JButton("Log out");
		btnSignOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				List<String> unArray=new ArrayList<String>();
				
				List<String> DDArray=new ArrayList<String>();
				List<String> DSArray=new ArrayList<String>();
				List<String> LDArray=new ArrayList<String>();
				List<String> LSArray=new ArrayList<String>();
				
				mygdToday();
				
				File currentFile=new File("..\\..\\Hotel\\Maintenance\\Current Available");
				currentFile.mkdirs();
				
				File unAvailableFile=new File("..\\..\\Hotel\\Maintenance\\unavailable.txt");
				
				try {
					
					Scanner myUnScanner=new Scanner(unAvailableFile);
					while(myUnScanner.hasNext()) {
						unArray.add(myUnScanner.next()+"\n");
					
					}
					
					
					
				}catch(Exception ex){
					JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
				}
				
				File DDFolder=new File("..\\..\\Hotel\\Unbooked\\Delux Double Room\\"+gdToday+".txt");
				File DSFolder=new File("..\\..\\Hotel\\Unbooked\\Delux Single Room\\"+gdToday+".txt");
				File LDFolder=new File("..\\..\\Hotel\\Unbooked\\Luxury Double Room\\"+gdToday+".txt");
				File LSFolder=new File("..\\..\\Hotel\\Unbooked\\Luxury Single Room\\"+gdToday+".txt");
				
				File DDUnBookedUnAvailable=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\Delux Double Room_"+gdToday+".txt");
				File DSUnBookedUnAvailable=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\Delux Single Room_"+gdToday+".txt");
				File LDUnBookedUnAvailable=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\Luxury Double Room_"+gdToday+".txt");
				File LSUnBookedUnAvailable=new File("..\\..\\Hotel\\Maintenance\\not-in-unbooked_unavailable\\Luxury Single Room_"+gdToday+".txt");
				
				
				
				if(DDFolder.exists()) {
					
					try {
						Scanner DDScanner=new Scanner(DDFolder).useDelimiter(",\\s*");
						while(DDScanner.hasNext()){
							DDArray.add(DDScanner.next()+"\n");
						}
						
						
						DDArray.removeAll(unArray);
						//System.out.println("Got: "+DDArray);
						
						
						
						File currentDD=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Delux Double.txt");
						FileWriter DDWriter=new FileWriter(currentDD);
						BufferedWriter BFDDWriter=new BufferedWriter(DDWriter);
						
						
						
						
						for(String s:DDArray) {
							
							BFDDWriter.write(s);
							
						}
						BFDDWriter.close();
						DDWriter.close();
						

						
					} catch (Exception e1) {
						
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
				
					
				
					
				}else if(DDUnBookedUnAvailable.exists()) {
					
					try {
						Scanner DDScanner=new Scanner(DDUnBookedUnAvailable);
						while(DDScanner.hasNext()){
							DDArray.add(DDScanner.next()+"\n");
						}
						
						
						DDArray.removeAll(unArray);
						//System.out.println("Got Exception: "+DDArray);
						
						
						
						File currentDD=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Delux Double.txt");
						FileWriter DDWriter=new FileWriter(currentDD);
						BufferedWriter BFDDWriter=new BufferedWriter(DDWriter);
						
						
						
						
						for(String s:DDArray) {
							
							BFDDWriter.write(s);
							
						}
						BFDDWriter.close();
						DDWriter.close();
						

						
					} catch (Exception e1) {
						
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
				

					
				}
				
				if(DSFolder.exists()) {
					
					try {
						
						Scanner DSScanner=new Scanner(DSFolder).useDelimiter(",\\s*");
						while(DSScanner.hasNext()){
							DSArray.add(DSScanner.next()+"\n");
						}
						
						
						DSArray.removeAll(unArray);
						//System.out.println("Got: "+DSArray);
						
						
						
						File currentDS=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Delux Single.txt");
						FileWriter DSWriter=new FileWriter(currentDS);
						BufferedWriter BFDSWriter=new BufferedWriter(DSWriter);
						
						
						
						
						for(String s:DSArray) {
							
							BFDSWriter.write(s);
							
						}
						BFDSWriter.close();
						DSWriter.close();
						
						
						
						
						
					}catch(Exception ex) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
					
				}else if(DSUnBookedUnAvailable.exists()){
					
					
					
					try {
						
						Scanner DSScanner=new Scanner(DSUnBookedUnAvailable);
						while(DSScanner.hasNext()){
							DSArray.add(DSScanner.next()+"\n");
						}
						
						
						DSArray.removeAll(unArray);
						//System.out.println("Got: "+DSArray);
						
						
						
						File currentDS=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Delux Single.txt");
						FileWriter DSWriter=new FileWriter(currentDS);
						BufferedWriter BFDSWriter=new BufferedWriter(DSWriter);
						
						
						
						
						for(String s:DSArray) {
							
							BFDSWriter.write(s);
							
						}
						BFDSWriter.close();
						DSWriter.close();
						
						
						
						
						
					}catch(Exception ex) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
					
	
					
				}
				
                if(LDFolder.exists()) {
					
					try {
						
						Scanner LDScanner=new Scanner(LDFolder).useDelimiter(",\\s*");
						while(LDScanner.hasNext()){
							LDArray.add(LDScanner.next()+"\n");
						}
						
						
						LDArray.removeAll(unArray);
						//System.out.println("Got: "+LDArray);
						
						
						
						File currentLD=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Luxury Double.txt");
						FileWriter LDWriter=new FileWriter(currentLD);
						BufferedWriter BFLDWriter=new BufferedWriter(LDWriter);
						
						
						
						
						for(String s:LDArray) {
							
							BFLDWriter.write(s);
							
						}
						BFLDWriter.close();
						LDWriter.close();
						
						
						
						
						
					}catch(Exception ex) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
					
				}else if(LDUnBookedUnAvailable.exists()) {
					
					
					try {
						
						Scanner LDScanner=new Scanner(LDUnBookedUnAvailable);
						while(LDScanner.hasNext()){
							LDArray.add(LDScanner.next()+"\n");
						}
						
						
						LDArray.removeAll(unArray);
						//System.out.println("Got: "+LDArray);
						
						
						
						File currentLD=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Luxury Double.txt");
						FileWriter LDWriter=new FileWriter(currentLD);
						BufferedWriter BFLDWriter=new BufferedWriter(LDWriter);
						
						
						
						
						for(String s:LDArray) {
							
							BFLDWriter.write(s);
							
						}
						BFLDWriter.close();
						LDWriter.close();
						
						
						
						
						
					}catch(Exception ex) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
					
					
					
					
				}
                
                if(LSFolder.exists()) {
					
					try {
						
						Scanner LSScanner=new Scanner(LSFolder).useDelimiter(",\\s*");
						while(LSScanner.hasNext()){
							LSArray.add(LSScanner.next()+"\n");
							
							LSArray.removeAll(unArray);
							//System.out.println(LSArray);
							
							
							
							File currentLS=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Luxury Single.txt");
							FileWriter LSWriter=new FileWriter(currentLS);
							BufferedWriter BFLSWriter=new BufferedWriter(LSWriter);
							
							
							
							
							for(String s:LSArray) {
								
								BFLSWriter.write(s);
								
							}
							BFLSWriter.close();
							LSWriter.close();
							
							
						}
						
					}catch(Exception ex) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					
					
				}else if(LSUnBookedUnAvailable.exists()) {
					
					
					try {
						
						Scanner LSScanner=new Scanner(LSUnBookedUnAvailable);
						while(LSScanner.hasNext()){
							LSArray.add(LSScanner.next()+"\n");
							
							LSArray.removeAll(unArray);
							//System.out.println(LSArray);
							
							
							
							File currentLS=new File("..\\..\\Hotel\\Maintenance\\Current Available\\current Available Luxury Single.txt");
							FileWriter LSWriter=new FileWriter(currentLS);
							BufferedWriter BFLSWriter=new BufferedWriter(LSWriter);
							
							
							
							
							for(String s:LSArray) {
								
								BFLSWriter.write(s);
								
							}
							BFLSWriter.close();
							LSWriter.close();
							
							
						}
						
					}catch(Exception ex) {
						JOptionPane.showMessageDialog(comboBox, "System Error","Error Message",JOptionPane.ERROR_MESSAGE);
					}
					

					
				}
                
               
				
			}
		});
		btnSignOut.setBounds(1078, 603, 89, 23);
		frmHotelManagementSystemmanager.getContentPane().add(btnSignOut);
		
		

		
	}
}
