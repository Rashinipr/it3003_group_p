import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreateAcntIDinterface {

	public JFrame IDframe;
	private JTextField textID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateAcntIDinterface window = new CreateAcntIDinterface();
					window.IDframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CreateAcntIDinterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		IDframe = new JFrame();
		IDframe.setBounds(100, 100, 450, 300);
		IDframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		IDframe.getContentPane().setLayout(null);

		textID = new JTextField();
		textID.setBackground(new Color(255, 255, 255));
		textID.setBounds(299, 141, 111, 35);
		IDframe.getContentPane().add(textID);
		textID.setColumns(10);

		JLabel lblID = new JLabel("Please Enter the ID");
		lblID.setFont(new Font("Andale Mono", Font.PLAIN, 15));
		lblID.setForeground(new Color(255, 255, 255));
		lblID.setBounds(271, 103, 179, 16);
		IDframe.getContentPane().add(lblID);

		JButton btnConfirm = new JButton("CONFIRM");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textID.getText().equals("123")) {
					IDframe.setVisible(false);
					ManagerAcntInterface w3 = new ManagerAcntInterface();
					w3.Acntframe.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Incorrect ID");
				}
			}
		});
		btnConfirm.setFont(new Font("Bodoni 72 Oldstyle", Font.BOLD, 13));
		btnConfirm.setForeground(new Color(0, 0, 128));
		btnConfirm.setBounds(247, 222, 92, 35);
		IDframe.getContentPane().add(btnConfirm);

		JButton btnback = new JButton("BACK");
		btnback.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IDframe.setVisible(false);
				ManagerMainInterface w5 = new ManagerMainInterface();
				w5.Homeframe.setVisible(true);
			}
		});
		btnback.setForeground(new Color(0, 0, 128));
		btnback.setFont(new Font("Bodoni 72 Oldstyle", Font.BOLD, 13));
		btnback.setBounds(341, 222, 92, 35);
		IDframe.getContentPane().add(btnback);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("./src/images/password.jpeg"));
		lblNewLabel.setBounds(0, 0, 450, 278);
		IDframe.getContentPane().add(lblNewLabel);

	}
}
