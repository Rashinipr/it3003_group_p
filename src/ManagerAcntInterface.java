import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ManagerAcntInterface {

	public JFrame Acntframe;
	private JTextField textName;
	private JTextField textId;
	private JTextField textUsername;
	private JTextField textPassword;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManagerAcntInterface window = new ManagerAcntInterface();
					window.Acntframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ManagerAcntInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		Acntframe = new JFrame();
		Acntframe.getContentPane().setBackground(new Color(176, 196, 222));
		Acntframe.setBounds(100, 100, 450, 331);
		Acntframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Acntframe.getContentPane().setLayout(null);

		textName = new JTextField();
		textName.setBounds(168, 108, 174, 26);
		Acntframe.getContentPane().add(textName);
		textName.setColumns(10);

		textId = new JTextField();
		textId.setColumns(10);
		textId.setBounds(168, 146, 174, 26);
		Acntframe.getContentPane().add(textId);

		textUsername = new JTextField();
		textUsername.setColumns(10);
		textUsername.setBounds(168, 186, 174, 26);
		Acntframe.getContentPane().add(textUsername);

		textPassword = new JTextField();
		textPassword.setColumns(10);
		textPassword.setBounds(168, 224, 174, 26);
		Acntframe.getContentPane().add(textPassword);

		JButton btnSignUp = new JButton("SIGN UP");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = textUsername.getText();
				String password = textPassword.getText();
				String id = textId.getText();
				String name = textName.getText();
				if (username.equals("") || password.equals("") || id.equals("") || name.equals("")) {
					JOptionPane.showMessageDialog(null, "Please fill all required details");
				} else {
					File myfile = new File("./Login Details");
					myfile.mkdirs();
					File f = new File("./Login Details/" + username + ".txt");
					try {
						FileWriter wr = new FileWriter(f);
						wr.write(password);
						wr.close();

						JOptionPane.showMessageDialog(null, "Your Account has been created succesfully");
						Acntframe.setVisible(false);
						ManagerMainInterface w6 = new ManagerMainInterface();
						w6.Homeframe.setVisible(true);
					} catch (IOException ex) {
						System.out.println(ex);
					}
				}
			}
		});
		btnSignUp.setForeground(UIManager.getColor("Desktop.background"));
		btnSignUp.setFont(new Font("Yuanti TC", Font.BOLD, 15));
		btnSignUp.setBounds(97, 262, 117, 29);
		Acntframe.getContentPane().add(btnSignUp);

		JLabel lblName = new JLabel("Name         :");
		lblName.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblName.setForeground(new Color(0, 0, 128));
		lblName.setBounds(77, 108, 79, 16);
		Acntframe.getContentPane().add(lblName);

		JLabel lblID = new JLabel("ID               :");
		lblID.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblID.setForeground(new Color(0, 0, 128));
		lblID.setBounds(77, 146, 79, 16);
		Acntframe.getContentPane().add(lblID);

		JLabel lblUsename = new JLabel("Username :");
		lblUsename.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblUsename.setForeground(new Color(0, 0, 128));
		lblUsename.setBounds(77, 186, 91, 16);
		Acntframe.getContentPane().add(lblUsename);

		JLabel lblPassword = new JLabel("Password  :");
		lblPassword.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblPassword.setForeground(new Color(0, 0, 128));
		lblPassword.setBounds(77, 224, 79, 16);
		Acntframe.getContentPane().add(lblPassword);

		lblNewLabel = new JLabel("");
		lblNewLabel.setBackground(new Color(32, 178, 170));
		lblNewLabel.setIcon(new ImageIcon("./src/images/welcome.jpeg"));
		lblNewLabel.setBounds(31, 6, 387, 78);
		Acntframe.getContentPane().add(lblNewLabel);

		JButton btnBack = new JButton("HOME");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Acntframe.setVisible(false);
				ManagerMainInterface w4 = new ManagerMainInterface();
				w4.Homeframe.setVisible(true);
			}
		});
		btnBack.setForeground(UIManager.getColor("Desktop.background"));
		btnBack.setFont(new Font("Yuanti TC", Font.BOLD, 15));
		btnBack.setBounds(212, 262, 117, 29);
		Acntframe.getContentPane().add(btnBack);
		Acntframe.setVisible(true);
	}
}
