import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.border.EtchedBorder;


public class main_login_page{
	JFrame frmWelcome;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) { 
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main_login_page window = new main_login_page();
					window.frmWelcome.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main_login_page() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWelcome = new JFrame();
		frmWelcome.setResizable(false);
		frmWelcome.setTitle("Hotel Management System | Welcome!");
		frmWelcome.setAlwaysOnTop(true);
		frmWelcome.setBackground(new Color(255, 255, 255));
		frmWelcome.getContentPane().setForeground(new Color(0, 0, 0));
		frmWelcome.getContentPane().setBackground(new Color(255, 240, 245));
		frmWelcome.setBounds(400, 145, 581, 480);
		frmWelcome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWelcome.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				home_page window = new home_page();
				window.frmHotelAlizia.setVisible(true);
				frmWelcome.dispose();
				
			}
		});
		btnNewButton.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\user-group-icon (1).png"));
		btnNewButton.setBounds(74, 147, 161, 192);
		btnNewButton.setBorder(new EtchedBorder(EtchedBorder.RAISED, new Color(0, 0, 0), new Color(0, 0, 0)));
		frmWelcome.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\Manager-icon.png"));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				home_page window = new home_page();
//				window.frmHotelAlizia.setVisible(true);
				frmWelcome.dispose();
			}
		});
		btnNewButton_1.setBounds(315, 147, 161, 192);
		btnNewButton_1.setBorder(new EtchedBorder(EtchedBorder.RAISED, new Color(0, 0, 0), new Color(0, 0, 0)));
		frmWelcome.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("HOTEL  ALIZIA");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setToolTipText("");
		lblNewLabel.setForeground(new Color(128, 0, 0));
		lblNewLabel.setFont(new Font("Showcard Gothic", Font.BOLD, 40));
		lblNewLabel.setBounds(20, 15, 535, 84);
		frmWelcome.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel(" HOTEL  ALIZIA  ");
		lblNewLabel_2.setToolTipText("");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setForeground(new Color(233, 150, 122));
		lblNewLabel_2.setFont(new Font("Showcard Gothic", Font.BOLD, 40));
		lblNewLabel_2.setBounds(27, 14, 535, 92);
		frmWelcome.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Spend Quality Holidays with us.");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Segoe Print", Font.BOLD, 15));
		lblNewLabel_3.setBounds(271, 83, 247, 25);
		frmWelcome.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("25 Years of Excellence in Hospitality...");
		lblNewLabel_4.setForeground(new Color(240, 255, 255));
		lblNewLabel_4.setFont(new Font("Papyrus", Font.BOLD, 14));
		lblNewLabel_4.setBounds(222, 395, 277, 25);
		frmWelcome.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\H11 (1).jpg"));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(0, 0, 565, 441);
		frmWelcome.getContentPane().add(lblNewLabel_1);
		
	}

}
