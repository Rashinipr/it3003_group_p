import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.ImageIcon;

public class change_RoomDetails {

	private JFrame frame;
	private JRadioButton LS;
	private JRadioButton DS;
	private JRadioButton LD;
	private JRadioButton DD;	
	private final ButtonGroup bg = new ButtonGroup();
	static private JTextField textFieldPrice;
	static private JTextField textFieldHighLites;
	static String roomText;
	static JButton btnNewButton_7;
	private static Scanner myScanner;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					change_RoomDetails window = new change_RoomDetails();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public change_RoomDetails() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 455, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setBackground(new Color(153, 102, 102));
		panel_1_1.setBounds(0, 0, 443, 50);
		frame.getContentPane().add(panel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Change Room Details");
		lblNewLabel_1_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.BOLD, 25));
		panel_1_1.add(lblNewLabel_1_1_1);
		
		JPanel panel_1_2_2 = new JPanel();
		panel_1_2_2.setLayout(null);
		panel_1_2_2.setToolTipText("");
		panel_1_2_2.setBackground(new Color(250, 240, 230));
		panel_1_2_2.setBounds(0, 61, 443, 355);
		frame.getContentPane().add(panel_1_2_2);
		
		LS = new JRadioButton("Luxury Single");
		LS.setFont(new Font("Tempus Sans ITC", Font.BOLD, 12));
		LS.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				roomText="Luxury Single Room";
				File rDDFile =new File("..\\..\\Hotel\\Change R_Details\\"+ roomText +".txt");
				btnNewButton_7.setEnabled(true);
				if(rDDFile.exists() == true) {
					retriveData();
				}
				else {
					textFieldPrice.setText("");
					textFieldHighLites.setText("");
				}
			}
		});
		LS.setBackground(new Color(233, 150, 122));
		LS.setBounds(29, 38, 109, 23);
		bg.add(LS);
		panel_1_2_2.add(LS);
		
		DS = new JRadioButton("Deluxe Single ");
		DS.setFont(new Font("Tempus Sans ITC", Font.BOLD, 12));
		DS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				roomText="Deluxe Single Room";
				File rDDFile =new File("..\\..\\Hotel\\Change R_Details\\"+ roomText +".txt");
				btnNewButton_7.setEnabled(true);
				if(rDDFile.exists() == true) {
					retriveData();
				}
				else {
					textFieldPrice.setText("");
					textFieldHighLites.setText("");
				}
			}
		});
		DS.setBackground(new Color(233, 150, 122));
		DS.setBounds(29, 73, 109, 23);
		bg.add(DS);
		panel_1_2_2.add(DS);
		
		LD = new JRadioButton("Luxury Double");
		LD.setFont(new Font("Tempus Sans ITC", Font.BOLD, 12));
		LD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				roomText="Luxury Double Room";
				File rDDFile =new File("..\\..\\Hotel\\Change R_Details\\"+ roomText +".txt");
				btnNewButton_7.setEnabled(true);
				if(rDDFile.exists() == true) {
					retriveData();
				}
				else {
					textFieldPrice.setText("");
					textFieldHighLites.setText("");
				}
			}
		});
		LD.setBackground(new Color(233, 150, 122));
		LD.setBounds(29, 108, 109, 23);
		bg.add(LD);
		panel_1_2_2.add(LD);
		
		DD = new JRadioButton("Deluxe Double");
		DD.setFont(new Font("Tempus Sans ITC", Font.BOLD, 12));
		DD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				roomText="Deluxe Double Room";
				File rDDFile =new File("..\\..\\Hotel\\Change R_Details\\"+ roomText +".txt");
				btnNewButton_7.setEnabled(true);
				if(rDDFile.exists() == true) {
					retriveData();
				}
				else {
					textFieldPrice.setText("");
					textFieldHighLites.setText("");
				}
				
			}
		});
		
		DD.setBackground(new Color(233, 150, 122));
		DD.setBounds(29, 143, 109, 23);
		bg.add(DD);
		panel_1_2_2.add(DD);
		
	    btnNewButton_7 = new JButton("Update");
	    btnNewButton_7.setForeground(new Color(128, 0, 0));
	    btnNewButton_7.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
		btnNewButton_7.setEnabled(false);
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File RDFolder=new File("..\\..\\Hotel\\Change R_Details");
		        RDFolder.mkdirs();
		         
				String price=textFieldPrice.getText();
				String highLites=textFieldHighLites.getText();				
					
					File ddFile=new File("..\\..\\Hotel\\Change R_Details\\"+ roomText +".txt");
					
					try {
						FileWriter ddWriter=new FileWriter(ddFile);
						ddWriter.write("Price: "+ price +"\nRoom HighLights: "+ highLites + "\n");
						ddWriter.close();
						}
					catch(Exception ex){
						JOptionPane.showMessageDialog(null,"ERROR: File Not Found","title", JOptionPane.INFORMATION_MESSAGE);
						}
					
					textFieldPrice.setText("");
					textFieldHighLites.setText("");
					
					JOptionPane.showMessageDialog(null,"Successfully Updated.","Alert!", JOptionPane.INFORMATION_MESSAGE);
					
					DD.setSelected(false);
					LS.setSelected(false);
					DS.setSelected(false);
					LD.setSelected(false);
			}
		});
		btnNewButton_7.setBounds(228, 240, 159, 44);
		panel_1_2_2.add(btnNewButton_7);
		
		textFieldPrice = new JTextField();
		textFieldPrice.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 14));
		textFieldPrice.setBounds(210, 54, 223, 32);
		panel_1_2_2.add(textFieldPrice);
		textFieldPrice.setColumns(10);
		
		textFieldHighLites = new JTextField();
		textFieldHighLites.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 13));
		textFieldHighLites.setBounds(210, 106, 223, 32);
		panel_1_2_2.add(textFieldHighLites);
		textFieldHighLites.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\price.jpg"));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(164, 54, 36, 32);
		panel_1_2_2.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setIcon(new ImageIcon("C:\\IT3003_HMS_groupP\\Images\\fac.jpg"));
		lblNewLabel_1.setBounds(164, 108, 36, 32);
		panel_1_2_2.add(lblNewLabel_1);
		
	}
	
	static void retriveData() {
		btnNewButton_7.setEnabled(true);
		File rDDFile =new File("..\\..\\Hotel\\Change R_Details\\"+ roomText +".txt");
		
		try {
			myScanner = new Scanner(rDDFile);
			textFieldPrice.setText(myScanner.nextLine());
			textFieldHighLites.setText(myScanner.nextLine());
			}
		catch(Exception ex) {
			JOptionPane.showMessageDialog(null,"ERROR: File Not Found","Alert!", JOptionPane.INFORMATION_MESSAGE);
			}
	}

}
