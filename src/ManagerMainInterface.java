import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
//import javax.swing.UIManager;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManagerMainInterface {

	public JFrame Homeframe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManagerMainInterface window = new ManagerMainInterface();
					window.Homeframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ManagerMainInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		Homeframe = new JFrame();
		Homeframe.getContentPane().setBackground(new Color(70, 130, 180));
		Homeframe.setBounds(100, 100, 450, 303);
		Homeframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Homeframe.getContentPane().setLayout(null);

		JButton btnLoginMain = new JButton("Login");
		btnLoginMain.setOpaque(true);
		btnLoginMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Homeframe.setVisible(false);
				ManagerLoginInterface w = new ManagerLoginInterface();
				w.Loginframe.setVisible(true);
			}
		});
		btnLoginMain.setBackground(new Color(0, 0, 0));
		btnLoginMain.setFont(new Font("Songti SC", Font.BOLD, 15));
		btnLoginMain.setForeground(new Color(0, 0, 128));
		btnLoginMain.setBounds(78, 185, 128, 38);
		Homeframe.getContentPane().add(btnLoginMain);

		JButton btnCreateAcntMain = new JButton("Create an Account");
		btnCreateAcntMain.setOpaque(true);
		btnCreateAcntMain.setBackground(new Color(0, 0, 0));
		btnCreateAcntMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Homeframe.setVisible(false);
				CreateAcntIDinterface w1 = new CreateAcntIDinterface();
				w1.IDframe.setVisible(true);
			}
		});
		btnCreateAcntMain.setFont(new Font("Songti SC", Font.BOLD, 15));
		btnCreateAcntMain.setForeground(new Color(0, 0, 128));
		btnCreateAcntMain.setBounds(78, 235, 128, 38);
		Homeframe.getContentPane().add(btnCreateAcntMain);

		JButton btnHistory = new JButton("Login History");
		btnHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String password = JOptionPane.showInputDialog("Enter the Password to access the history");
				if (password.equals("abc123")) {
					Homeframe.setVisible(false);
					LoginHistory w = new LoginHistory();
					w.frame.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Incorrect password");
				}

			}
		});
		btnHistory.setOpaque(true);
		btnHistory.setForeground(new Color(0, 0, 128));
		btnHistory.setFont(new Font("Songti SC", Font.BOLD, 15));
		btnHistory.setBackground(new Color(0, 0, 0));
		btnHistory.setBounds(247, 185, 128, 38);
		Homeframe.getContentPane().add(btnHistory);

		JButton btnDelete = new JButton("Delete Account");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String password = JOptionPane.showInputDialog("Enter the Password");
				if (password.equals("abc123")) {
					Homeframe.setVisible(false);
					DeleteAcntInterface w = new DeleteAcntInterface();
					w.Deleteframe.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Incorrect password");
				}
			}
		});
		btnDelete.setOpaque(true);
		btnDelete.setForeground(new Color(0, 0, 128));
		btnDelete.setFont(new Font("Songti SC", Font.BOLD, 15));
		btnDelete.setBackground(new Color(0, 0, 0));
		btnDelete.setBounds(247, 235, 128, 38);
		Homeframe.getContentPane().add(btnDelete);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("./src/images/home.jpeg"));
		lblNewLabel.setBounds(0, 0, 450, 280);
		Homeframe.getContentPane().add(lblNewLabel);

	}

}
