import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
//import java.util.Scanner;
import java.awt.Color;
//import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class LoginHistory {

	public JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginHistory window = new LoginHistory();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginHistory() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(112, 128, 144));
		frame.setBounds(100, 100, 450, 328);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblLoginHistory = new JLabel("LOGIN HISTORY");
		lblLoginHistory.setForeground(new Color(255, 255, 255));
		lblLoginHistory.setFont(new Font("Nanum Gothic", Font.BOLD, 16));
		lblLoginHistory.setBounds(161, 6, 139, 16);
		frame.getContentPane().add(lblLoginHistory);

		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setBounds(36, 34, 379, 209);
		frame.getContentPane().add(textArea);
		
		populateHistory(textArea);

		JButton btnHome = new JButton("HOME");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				ManagerMainInterface w = new ManagerMainInterface();
				w.Homeframe.setVisible(true);
			}
		});
		btnHome.setBackground(UIManager.getColor("ComboBox.buttonDarkShadow"));
		btnHome.setOpaque(true);
		btnHome.setBounds(161, 257, 117, 29);
		frame.getContentPane().add(btnHome);

	}

	private void populateHistory(JTextArea textArea) {
		String Line = "";
		String data = "";
		try {

			BufferedReader br = new BufferedReader(new FileReader("./Login History/history.txt"));
			while (Line != null) {
				data += Line + "\n";
				Line = br.readLine();
			}
			br.close();
			textArea.append(data);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "No logins so far");
		}

		
	}
}
