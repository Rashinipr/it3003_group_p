import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class DeleteAcntInterface {

	public JFrame Deleteframe;
	private JTextField textUsername;
	private JTextField textPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteAcntInterface window = new DeleteAcntInterface();
					window.Deleteframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DeleteAcntInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		Deleteframe = new JFrame();
		Deleteframe.getContentPane().setBackground(new Color(255, 255, 255));
		Deleteframe.setBounds(100, 100, 450, 300);
		Deleteframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Deleteframe.getContentPane().setLayout(null);

		JLabel lblwarn = new JLabel("**If you delete your account, it will be permanently \ndeleted. ");
		lblwarn.setFont(new Font("Symbol", Font.BOLD, 14));
		lblwarn.setForeground(new Color(220, 20, 60));
		lblwarn.setBounds(33, 87, 411, 25);
		Deleteframe.getContentPane().add(lblwarn);

		JLabel lblNewLabel = new JLabel(" and you won't be abale to log in again**");
		lblNewLabel.setFont(new Font("Symbol", Font.BOLD, 14));
		lblNewLabel.setForeground(new Color(220, 20, 60));
		lblNewLabel.setBounds(6, 113, 297, 16);
		Deleteframe.getContentPane().add(lblNewLabel);

		JLabel lblimage = new JLabel("");
		lblimage.setIcon(new ImageIcon("./src/images/delete.jpeg"));
		lblimage.setBounds(126, 0, 177, 75);
		Deleteframe.getContentPane().add(lblimage);

		JLabel lblUsername = new JLabel(" Username :");
		lblUsername.setForeground(new Color(0, 0, 139));
		lblUsername.setBounds(16, 149, 99, 16);
		Deleteframe.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password  :");
		lblPassword.setForeground(new Color(0, 0, 139));
		lblPassword.setBounds(20, 178, 83, 16);
		Deleteframe.getContentPane().add(lblPassword);

		textUsername = new JTextField();
		textUsername.setBounds(102, 144, 130, 26);
		Deleteframe.getContentPane().add(textUsername);
		textUsername.setColumns(10);

		textPassword = new JTextField();
		textPassword.setBounds(102, 177, 130, 26);
		Deleteframe.getContentPane().add(textPassword);
		textPassword.setColumns(10);

		JButton btnDelete = new JButton("Delete Account");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = textUsername.getText();
				String password = textPassword.getText();
				String Line = "";
				String data = "";
				if (username.equals("") || password.equals("")) {
					JOptionPane.showMessageDialog(null, "Please fill all required details");
				} else {
					try {

						BufferedReader br = new BufferedReader(new FileReader("./Login Details/" + username + ".txt"));
						while (Line != null) {
							data += Line;
							Line = br.readLine();
						}
						br.close();
						if (password.equals(data)) {
							File f = new File("./Login Details/" + username + ".txt");
							f.delete();
							JOptionPane.showMessageDialog(null, "Your Account has been Deleted");
							Deleteframe.setVisible(false);
							ManagerMainInterface w = new ManagerMainInterface();
							w.Homeframe.setVisible(true);
						} else {
							JOptionPane.showMessageDialog(null, "Incorrect password or username");
						}
					} catch (IOException ex) {
						JOptionPane.showMessageDialog(null, "Incorrect password or username");
					}

				}
			}
		});
		btnDelete.setBackground(new Color(0, 0, 0));
		btnDelete.setOpaque(true);
		btnDelete.setFont(new Font("STIXSizeFiveSym", Font.BOLD, 14));
		btnDelete.setForeground(new Color(139, 0, 0));
		btnDelete.setBounds(73, 222, 140, 39);
		Deleteframe.getContentPane().add(btnDelete);

		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Deleteframe.setVisible(false);
				ManagerMainInterface w = new ManagerMainInterface();
				w.Homeframe.setVisible(true);
			}
		});
		btnHome.setOpaque(true);
		btnHome.setForeground(new Color(139, 0, 0));
		btnHome.setFont(new Font("STIXSizeFiveSym", Font.BOLD, 14));
		btnHome.setBackground(Color.BLACK);
		btnHome.setBounds(238, 222, 140, 39);
		Deleteframe.getContentPane().add(btnHome);
	}
}
