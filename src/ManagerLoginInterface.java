import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
//import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class ManagerLoginInterface {

	public JFrame Loginframe;
	private JTextField textUsername;
	private JTextField textPassword;
	private JButton btnLogin;
	private JButton btnBack;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManagerLoginInterface window = new ManagerLoginInterface();
					window.Loginframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ManagerLoginInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		Loginframe = new JFrame();
		Loginframe.getContentPane().setBackground(new Color(255, 255, 255));
		Loginframe.setBounds(100, 100, 450, 300);
		Loginframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Loginframe.getContentPane().setLayout(null);

		JLabel lblUsername = new JLabel("Username : ");
		lblUsername.setOpaque(true);
		lblUsername.setBackground(new Color(255, 255, 255));
		lblUsername.setForeground(new Color(0, 128, 128));
		lblUsername.setFont(new Font("Al Bayan", Font.BOLD, 15));
		lblUsername.setBounds(63, 110, 89, 16);
		Loginframe.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password  : ");
		lblPassword.setOpaque(true);
		lblPassword.setBackground(new Color(255, 255, 255));
		lblPassword.setForeground(new Color(0, 128, 128));
		lblPassword.setFont(new Font("Al Bayan", Font.BOLD, 15));
		lblPassword.setBounds(63, 148, 89, 16);
		Loginframe.getContentPane().add(lblPassword);

		textUsername = new JTextField();
		textUsername.setBounds(164, 100, 155, 34);
		Loginframe.getContentPane().add(textUsername);
		textUsername.setColumns(10);

		textPassword = new JTextField();
		textPassword.setBounds(164, 138, 155, 34);
		Loginframe.getContentPane().add(textPassword);
		textPassword.setColumns(10);

		btnLogin = new JButton("LOGIN");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = textUsername.getText();
				String password = textPassword.getText();
				String Line = "";
				String data = "";
				String LineN = "";
				String dataN = "";
				if (username.equals("") || password.equals("")) {
					JOptionPane.showMessageDialog(null, "Please fill all required details");
				} else {
					File myFile = new File("./Login History");
					myFile.mkdirs();
					File fl = new File("./Login History/history.txt");

					try {

						if (fl.createNewFile()) {
							// System.out.println(fl.getAbsolutePath());
						}
						BufferedReader br = new BufferedReader(new FileReader("./Login Details/" + username + ".txt"));
						while (Line != null) {
							data += Line;
							Line = br.readLine();
						}
						br.close();
						if (password.equals(data)) {
							Loginframe.setVisible(false);
							JOptionPane.showMessageDialog(null, "Logged in succesfully");
							DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
							LocalDateTime now = LocalDateTime.now();
							try {
								BufferedReader b = new BufferedReader(new FileReader(fl));
								while (LineN != null) {
									dataN += LineN + "\n";
									LineN = b.readLine();
								}
								FileWriter w = new FileWriter(fl);
								w.write(dataN);
								w.write(username + " " + dtf.format(now));
								b.close();
								w.close();

							} catch (IOException ex) {
								System.out.println(ex);
							}
						} else {
							JOptionPane.showMessageDialog(null, "Incorrect Password");
						}
					}

					catch (IOException ex) {
						JOptionPane.showMessageDialog(null, "Incorrect Username");
					}

				}
			}
		});
		btnLogin.setBackground(new Color(95, 158, 160));
		btnLogin.setOpaque(true);
		btnLogin.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		btnLogin.setForeground(new Color(47, 79, 79));
		btnLogin.setBounds(69, 222, 117, 29);
		Loginframe.getContentPane().add(btnLogin);

		btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Loginframe.setVisible(false);
				ManagerMainInterface w2 = new ManagerMainInterface();
				w2.Homeframe.setVisible(true);
			}
		});
		btnBack.setBackground(new Color(95, 158, 160));
		btnBack.setOpaque(true);
		btnBack.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		btnBack.setForeground(new Color(47, 79, 79));
		btnBack.setBounds(257, 222, 117, 29);
		Loginframe.getContentPane().add(btnBack);

		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("./src/images/loginimage.jpeg"));
		lblNewLabel.setBounds(70, 0, 283, 66);
		Loginframe.getContentPane().add(lblNewLabel);

	}

}
